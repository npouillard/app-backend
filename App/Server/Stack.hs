{- Copyright (C) Nicolas Pouillard - All Rights Reserved
   Refer to the LICENSE file.
-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE TypeOperators              #-}
module App.Server.Stack where

import App.Core
import App.Api
import App.Arbitrary
import App.Persist
import App.Server
import Control.Concurrent.MVar
import Control.Monad.Except
import Control.Monad.Reader
import Control.Monad.RWS.Lazy
import Data.ByteString.Builder
import Data.Foldable (toList)
import Data.JsonState
import Data.Swagger (Swagger)
import Data.Time.Clock
import Data.Time.Units
import Network.Wai
import Network.Wai.Middleware.Cors hiding (Origin)
-- import Network.Wai.Application.Static
import Servant
-- import System.FilePath (addTrailingPathSeparator)
import System.IO

import qualified Data.ByteString.Lazy as LBS
import qualified Data.Vector as V

-- The state is transactional since an exception throws away the state
newtype Stack m a = Stack { unStack :: RWST PersistEnv [Update] DB (ExceptT ServantErr m) a }
  deriving (Functor, Applicative, Monad, MonadError ServantErr, MonadState DB,
            MonadReader PersistEnv, MonadWriter [Update], MonadIO)

instance Monad m => MonadUpdate (Stack m)

type ServantM = ExceptT ServantErr IO

type Top = "api" :> Api
       :<|> "swagger.json" :> GetJ Swagger
       {-
       :<|> "api.js" :> Get '[PlainText] ST
       :<|> "static" :> Raw
       :<|> "swagger-ui" :> Raw
       -}

topApi :: Proxy Top
topApi = Proxy

appCorsResourcePolicy :: CorsConfig -> CorsResourcePolicy
appCorsResourcePolicy cfg = simpleCorsResourcePolicy
  { corsRequestHeaders  = ["content-type", "x-auth-token", "x-auth-id"]
  , corsMethods         = ["GET", "HEAD", "POST", "PUT", "PATCH", "DELETE"]
  , corsOrigins         = cfg ^? _FromOrigin . to (\os -> ((cs . fromOrigin) <$> toList os, False)) }

appCors :: CorsConfig -> Middleware
appCors = cors . const . Just . appCorsResourcePolicy

mkApp :: Config -> IO Application
mkApp cfg = do
  runStack <- mkRunStack cfg $ Nat (pure . runIdentity)
  let
    serveTop :: Server Top
    serveTop =  enter runStack (serveApi $ cfg ^. confAuthSeed)
           :<|> pure swaggerApi
           {-
           :<|> pure (cs apiJS)
           :<|> waiServeDirectory "./static"
           :<|> staticApp (defaultFileServerSettings "./swagger-ui")
           -}
  pure . appCors (cfg ^. confCors) $ serve topApi serveTop

-- FIXME catch IO exceptions to save state (in case of termination)
mkRunStack :: forall m. Config -> m :~> IO -> IO (Stack m :~> ServantM)
mkRunStack cfg runBase = do
  let dbJson = cfg ^. confDbDumpJson
  loadedState <-
    if dbJson == "/dev/null" then
      Right <$> generateRandomDB 1
    else
      loadState dbJson
  case loadedState of
    Left (b, m) -> do
      putStrLn $ "Failed " <> if b then "parsing" else "reading" <>
                 " of JSON file: " <> dbJson
      fail m
    Right initState -> do
      v <- newMVar initState
      save <- mkSaveState (fromIntegral (cfg ^. confSaveRateSeconds) :: Second) dbJson
      let
        go :: Stack m a -> ServantM a
        go (Stack m) = do
          now <- getTimestampIO
          s0 <- liftIO $
            if cfg ^. confOptimisticLock then
              -- This will not block for long only the time to write down the
              -- log entry.
              readMVar v
            else
              -- This `takeMVar` corresponds to a global lock which is safe but inefficient.
              takeMVar v
          (a,s1,ups) <- ExceptT . unNat runBase . runExceptT $ runRWST m (PersistEnv now) s0
          assertEqual "db version should not be changed" (s0 ^. version) (s1 ^. version)
          if null ups then pure a
          else do
            -- Some updates took place so we must save the new state
            let s2 = s1 & version %~ succ
                finish = liftIO $ do
                  logUpdates (cfg ^. confDbLogJson) $ LogEntry (V.fromList ups) (s1 ^. version)
                  save s2
                  putMVar v s2 -- Now releasing the lock for other clients to read.
                  pure a
            if cfg ^. confOptimisticLock then do
              s3 <- liftIO $ takeMVar v
              if s3 ^. version /= s0 ^. version then do
                -- Arg some concurrent write took place we have to retry
                liftIO $ putMVar v s3
                go (Stack m)
              else
                -- Good, no concurrent write took place so we can finish
                finish
            else
              finish
      pure $ Nat go

logUpdates :: FilePath -> LogEntry -> IO ()
logUpdates logJson = LBS.appendFile logJson . (<> "\n") . encode

emptyDB :: DB
emptyDB = DB nil nil nil 1 (DbVersion 1)

getTimestampIO :: MonadIO m => m Timestamp
getTimestampIO = Timestamp <$> liftIO getCurrentTime

runEphermalStack :: Stack IO a -> IO (a, DB, [Update])
runEphermalStack (Stack m) = do
    now <- getTimestampIO
    er <- runExceptT $ runRWST m (PersistEnv now) emptyDB
    either (fail . show) pure er

generateAuthSeed :: IO AuthSeed
generateAuthSeed = AuthSeed . cs . toLazyByteString . byteStringHex . cs
                <$> withBinaryFile "/dev/random" ReadMode (replicateM 16 . hGetChar)

generateConfig :: IO Config
generateConfig = defaultConfig <$> generateAuthSeed

defaultMailgunConfig :: MailgunConfig
defaultMailgunConfig = MailgunConfig
  { _mailgunDomain = "SOME_DOMAIN"
  , _mailgunApikey = "SOME_APIKEY"
  , _mailgunSender = "no-reply@example.com"
  }

defaultConfig :: AuthSeed -> Config
defaultConfig seed = Config
  { _confListenHost      = "0.0.0.0"
  , _confListenPort      = 3080
  , _confSaveRateSeconds = 3
  , _confOptimisticLock  = True
  , _confDbDumpJson      = "db-dump.json"
  , _confDbLogJson       = "db-log.json"
  , _confMailgun         = defaultMailgunConfig
  , _confCors            = AnyOrigin
  , _confAuthSeed        = seed
  }

devConfig :: Config
devConfig = defaultConfig (AuthSeed "DEV SEED")
  & confDbDumpJson .~ "/dev/null"
  & confDbLogJson  .~ "/dev/null"

generateRandomDB :: Int -> IO DB
generateRandomDB = fmap (view _2) . runEphermalStack . populateDB

{-
waiServeDirectory :: FilePath -> Application
waiServeDirectory =
  staticApp . tweakStaticSettings .
  defaultFileServerSettings . addTrailingPathSeparator

tweakStaticSettings :: StaticSettings -> StaticSettings
tweakStaticSettings s = s { ssAddTrailingSlash = True }
-}
