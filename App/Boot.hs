{- Copyright (C) Nicolas Pouillard - All Rights Reserved
   Refer to the LICENSE file.
-}
module App.Boot where

import Data.Aeson.Types as Aeson
import Data.List
import Data.List.Split
import Data.Swagger as Swagger

modifier :: [String] -> String -> String
modifier exns = intercalate "_"
              . f
              . splitOn "_"
              . drop 1
              . camelTo2 '_'
  where
    f (x:y:zs) | x `elem` exns = f (y:zs)
    f xs                       = xs

newtypeOptions :: Options
newtypeOptions = defaultOptions
  { Aeson.fieldLabelModifier = error "newtypeOptions.fieldLabelModifier"
  , Aeson.constructorTagModifier = error "newtypeOptions.constructorTagModifier"
  , Aeson.unwrapUnaryRecords = True }

prefixOptions :: [String] -> Options
prefixOptions exns = defaultOptions
  { Aeson.fieldLabelModifier = modifier exns
  , Aeson.constructorTagModifier = camelTo2 '_'
  , Aeson.unwrapUnaryRecords = False
  , omitNothingFields  = True }

prefixSchemaOptions :: [String] -> SchemaOptions
prefixSchemaOptions exns = defaultSchemaOptions
  { Swagger.fieldLabelModifier = modifier exns
  , Swagger.constructorTagModifier = camelTo2 '_'
  , Swagger.unwrapUnaryRecords = True }

type Endom a = a -> a
