{- Copyright (C) Nicolas Pouillard - All Rights Reserved
   Refer to the LICENSE file.
-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

-- try again with lens >= 4.15.1
{-# OPTIONS -fno-warn-redundant-constraints #-}
module App.Person where
import Control.Lens
import Data.Aeson
import Data.Aeson.TH
import Data.Swagger.Internal.Schema (ToSchema(..), genericDeclareNamedSchema)
import Generics.Deriving.Monoid (memptydefault, mappenddefault)

import App.Common

data MetaPerson = MetaPerson
  { _mpID        :: !(ID Person)
  , _mpCreatedBy :: !Username
  , _mpCreatedAt :: !Timestamp
  , _mpChanges   :: ![Change Person]
  , _mpNonce     :: !Nonce
{- This nonce is a counter used to change the authentication tokens.
   Asking for a new token simply bumps this counter.
   This counter is mixed with the person id to make the authentication token.
-}
  }
  deriving (Eq, Ord, Show, Generic)

data Person = Person
  { _pGender     :: !Gender
  , _pLastname   :: !Lastname
  , _pFirstname  :: !Firstname
  , _pBirthdate  :: !(Maybe Birthdate)
  , _pEmail      :: !EmailAddress
  , _pArchived   :: !(Maybe Eventdate)
  }
  deriving (Eq, Ord, Show, Generic)

data ItemPerson = ItemPerson
  { _ipGender        :: !Gender
  , _ipLastname      :: !Lastname
  , _ipFirstname     :: !Firstname
  }
  deriving (Eq, Ord, Show, Generic)

type instance IDOf       Person = ID Person
type instance StoreMeta  Person = MetaPerson
type instance ViewMeta   Person = Meta (ID Person) Person
type instance ItemMeta   Person = MiniMeta (ID Person) Person
type instance StoreData  Person = Person
type instance NewData    Person = Person
type instance UpdateData Person = Person
type instance ViewData   Person = Person
type instance ItemData   Person = ItemPerson
type instance Plural     Person = "people"
type instance Singular   Person = "person"

data PersonQuery = PersonQuery
  { _anyPersonID      :: !(Maybe (Vector (ID Person)))
  , _anyGender        :: !(Maybe (Vector Gender))
  , _anyBirthdate     :: !(Maybe (Vector (Comparator Birthdate)))
  , _anyArchived      :: !(Maybe (Vector (Comparator Eventdate)))
  }
  deriving (Eq, Ord, Show, Generic)

data Query = Query { _queryAll :: Maybe PersonQuery
                   , _queryNot :: Maybe PersonQuery
                   }
  deriving (Show, Eq, Ord, Generic)

deriveJSON (prefixOptions ["any"])            ''PersonQuery
deriveJSON (prefixOptions ["ip"])             ''ItemPerson
deriveJSON (prefixOptions ["mp"])             ''MetaPerson
deriveJSON (prefixOptions ["p"])              ''Person
deriveJSON (prefixOptions ["query"])          ''Query

instance ToSchema PersonQuery       where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions ["any"]
instance ToSchema MetaPerson        where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions ["mp"]
instance ToSchema ItemPerson        where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions ["ip"]
instance ToSchema Person            where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions ["p"]
instance ToSchema Query             where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions ["query"]

makePrisms ''Person

makeLenses ''Person
makeLenses ''PersonQuery
makeLenses ''MetaPerson
makeLenses ''ItemPerson
makeLenses ''Query

instance HasID Person   MetaPerson      where _ID = mpID

instance HasChanges Person   MetaPerson     where changes _ = mpChanges

instance HasCreatedBy Person   MetaPerson     where createdBy _ = mpCreatedBy

instance HasCreatedAt Person   MetaPerson     where createdAt _ = mpCreatedAt

instance HasUsername Person            where username = to $ Username . view fullname -- fullname does not start with '@'
instance HasUsername ItemPerson        where username = to $ Username . view fullname -- fullname does not start with '@'

instance HasEmailAddress Person        where emailAddress = pEmail

instance HasNonce MetaPerson        where nonce = mpNonce

instance HasGender Person           where gender = pGender
instance HasGender ItemPerson       where gender = ipGender

instance HasFirstname Person        where firstname = pFirstname
instance HasFirstname ItemPerson    where firstname = ipFirstname

instance HasLastname Person         where lastname  = pLastname
instance HasLastname ItemPerson     where lastname  = ipLastname

personP :: Proxy Person
personP = Proxy

instance HasView Person where
  viewMeta _ m _ = like Meta
    { _metaID        = m ^. mpID
    , _metaCreatedBy = m ^. mpCreatedBy
    , _metaCreatedAt = m ^. mpCreatedAt
    , _metaChanges   = m ^. mpChanges
    }

instance HasItem Person where
  itemData _ p = ItemPerson
    { _ipGender         = p ^. gender
    , _ipFirstname      = p ^. firstname
    , _ipLastname       = p ^. lastname
    }

-- Deletion is final
instance HasDelete Person

instance HasUpdate Person

instance HasNew Person where
  newMeta _ _ r = MetaPerson
    { _mpID        = m ^. metaID
    , _mpCreatedBy = m ^. metaCreatedBy
    , _mpCreatedAt = m ^. metaCreatedAt
    , _mpChanges   = m ^. metaChanges
    , _mpNonce     = 0
    } where m = r ^. _meta

instance Monoid PersonQuery where
  mempty = memptydefault
  mappend = mappenddefault

instance Monoid Query where
  mempty = memptydefault
  mappend = mappenddefault

matchPerson :: ([Bool] -> Bool) -> PersonQuery -> StoreRecord Person -> Bool
matchPerson combine
            q@(PersonQuery _ _ _ _keepmeupdated)
            p@(Record _ (Person _ _ _ _ _ _also)) =
  combine . concat $
      [ match        q (anyPersonID      . _Just . each) p (_meta . _ID)
      , match        q (anyGender        . _Just . each) p (_data . pGender)
      , matchWithCmp q (anyBirthdate     . _Just . each) p (_data . pBirthdate . _Just)
      , matchEndDate q (anyArchived      . _Just . each) p (_data . pArchived) id
      ]

selectPeople :: Maybe PersonQuery
             -> Traversal' (StoreRecord Person) (StoreRecord Person)
selectPeople = \case
  Nothing -> id
  Just q  -> filtered (matchPerson and q)

unselectPeople :: Maybe PersonQuery
               -> Traversal' (StoreRecord Person) (StoreRecord Person)
unselectPeople = \case
  Nothing -> id
  Just q  -> filtered (not . matchPerson or q)
