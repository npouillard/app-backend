{- Copyright (C) Nicolas Pouillard - All Rights Reserved
   Refer to the LICENSE file.
-}
{-# LANGUAGE OverloadedStrings #-}
module App.Logger (logInfo, logError) where

import Data.Aeson
import Data.String.Conversions (ST)
import Data.Time.Clock (getCurrentTime)
import Data.Time.Format (formatTime, defaultTimeLocale)
import System.IO

import qualified Data.ByteString.Lazy.Char8 as LBS

-- This matches what RequestLogger.JSON does.
loggerTimeFormat :: String
loggerTimeFormat = "%d/%b/%Y:%H:%M:%S +0000"

-- Config is not used yet but could have a logging level for instance.
baseLogger :: ToJSON body => ST -> config -> ST -> body -> IO ()
baseLogger ty _cfg code body = do
  now <-formatTime defaultTimeLocale loggerTimeFormat <$> getCurrentTime
  LBS.hPutStrLn stdout . encode $
    object ["time" .= toJSON now,
            "type" .= String ty,
            "code" .= String code,
            "body" .= toJSON body]

logInfo :: ToJSON body => config -> ST -> body -> IO ()
logInfo = baseLogger "info"

logError :: ToJSON body => config -> ST -> body -> IO ()
logError = baseLogger "error"
