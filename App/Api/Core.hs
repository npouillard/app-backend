{- Copyright (C) Nicolas Pouillard - All Rights Reserved
   Refer to the LICENSE file.
-}
{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE TypeOperators      #-}
module App.Api.Core where

import App.Record (ItemRecord, NewData, UpdateData, ViewRecord, IDOf, Singular, Plural)
import Servant.API

type GetJ    a = Get             '[JSON] a
type PutJ    a = Put             '[JSON] a
type PostJ   a = Post            '[JSON] a
type NewJ    a = PostCreated     '[JSON] a
type PatchJ  a = Patch           '[JSON] a
type BodyJ   a = ReqBody         '[JSON] a
type DeleteJ   = DeleteNoContent '[JSON] ()
type VoidJ     = PostNoContent   '[JSON] ()

-- Resources
type View    k = GetJ (ViewRecord k)
type Edit    k = BodyJ (UpdateData k) :> PutJ (ViewRecord k)
type Del     k = DeleteJ
type Restore k = VoidJ

-- Collections
type New    k = BodyJ (NewData k) :> NewJ (ViewRecord k)
type Import k = BodyJ [NewData k] :> NewJ [ViewRecord k]
type List   k = GetJ [ItemRecord k]
type Export k = GetJ [ViewRecord k]

type CaptureID k = Capture (Singular k) (IDOf k)

type Resource k
    =  View    k
  :<|> Edit    k
  :<|> Del     k
  :<|> Restore k

type Collection k =
    (  Plural   k :> List k
  :<|> Plural   k :> "export" :> Export k
  :<|> Plural   k :> "import" :> Import k
  :<|> Plural   k :> New k
  :<|> Singular k :> CaptureID k :> Resource k
    )

type ViewCollection k =
    (  Plural   k :> List k
  :<|> Plural   k :> "export" :> Export k
  :<|> Singular k :> CaptureID k :> View k
    )
