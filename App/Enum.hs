{- Copyright (C) Nicolas Pouillard - All Rights Reserved
   Refer to the LICENSE file.
-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# OPTIONS -fno-warn-redundant-constraints #-}

-- So far we edit options individually:
-- * this is more efficient and more RESTy
-- * we have no support for ordering the option

-- Editing the options as a list would:
-- * allow reordering
-- * the UI could be nice for intensive changes in a list (and we have all the
--   needed widgets)
-- * Needs some checks that the new list is not screwing up the data.
--   In particular now that the representation has some meta data about
--   each option these should be merged with care while updating.
--   This looks like a lens (in the sense of update-of-a-view) problem.

-- What are the allowed actions:
-- * Add a new option to an enum (pick the id ? pick the text)
-- * Change the text of an option
-- * Archive an option
-- * Unarchive an option (TODO)

-- Not allowed:
-- * Remove an option entierly
-- * Change an ID
-- * Add a new option with a conflicting ID
-- * Pick an insane ID (the UI enforces [a-zA-Z0-1]+
module App.Enum where

import App.Common
import Data.Aeson.TH
import Data.Swagger.Internal.Schema (ToSchema(..), genericDeclareNamedSchema, unname)
import Data.Swagger (ToParamSchema)
import Prelude hiding (Enum)
import Web.HttpApiData (ToHttpApiData, FromHttpApiData)

newtype OptionID = OptionID ST
  deriving (Eq, Ord, Show, Generic, ToJSON, FromJSON, FromHttpApiData, ToHttpApiData)

newtype EnumID = EnumID ST
  deriving (Eq, Ord, Show, Generic, ToJSON, FromJSON, FromHttpApiData, ToHttpApiData)

data Option = Option
  { _optName :: !ST
  }
  deriving (Eq, Ord, Show, Generic)

data NewOption = NewOption
  { _noID   :: !OptionID
  , _noName :: !ST
  }
  deriving (Eq, Ord, Show, Generic)

data UpdateOption = UpdateOption
  { _uoName :: !ST
  }
  deriving (Eq, Ord, Show, Generic)

makeLenses ''Option
makeLenses ''NewOption
makeLenses ''UpdateOption

optionP :: Proxy Option
optionP = Proxy

type instance IDOf       Option = OptionID
type instance StoreMeta  Option = Meta OptionID Option
type instance ViewMeta   Option = Meta OptionID Option
type instance ItemMeta   Option = MiniMeta OptionID Option
type instance StoreData  Option = Option
type instance NewData    Option = NewOption
type instance UpdateData Option = UpdateOption
type instance ViewData   Option = Option
type instance ItemData   Option = Option
type instance Plural     Option = "options"
type instance Singular   Option = "option"

instance ToParamSchema OptionID
instance ToSchema OptionID     where declareNamedSchema = fmap unname . genericDeclareNamedSchema (prefixSchemaOptions [])
instance ToSchema Option       where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions ["opt"]
instance ToSchema NewOption    where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions ["no"]
instance ToSchema UpdateOption where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions ["uo"]

instance HasID Option NewOption where
  _ID = noID

instance HasID Option OptionID where
  _ID = id

instance HasUpdate Option where
  updateData _ (UpdateOption t) = optName .~ t

instance HasNew Option where
  newData _ no = Option { _optName = no ^. noName }

instance HasView Option
instance HasItem Option

-- Deletion does nothing to the data.
-- It is known to be archived in the meta.changes
instance HasDelete Option where
  deleteRecord _ _ = Just

data Enum = Enum
  { _enumName    :: !(Maybe ST)
  , _enumOptions :: !(IDMap Option)
  }
  deriving (Eq, Show, Generic)

data NewEnum = NewEnum
  { _neID   :: !EnumID
  , _neName :: !(Maybe ST)
  }
  deriving (Eq, Ord, Show, Generic)

data UpdateEnum = UpdateEnum
  { _ueName :: !(Maybe ST)
  }
  deriving (Eq, Ord, Show, Generic)

data ItemEnum = ItemEnum
  { _ieName        :: !(Maybe ST)
  , _ieOptionCount :: !Count
  }
  deriving (Eq, Ord, Show, Generic)

makeLenses ''Enum
makeLenses ''NewEnum
makeLenses ''UpdateEnum
makeLenses ''ItemEnum

optionCount :: Getter Enum Count
optionCount = to $ Count . lengthOf (enumOptions . each)

enumP :: Proxy Enum
enumP = Proxy

type instance IDOf       Enum = EnumID
type instance StoreMeta  Enum = Meta EnumID Enum
type instance ViewMeta   Enum = Meta EnumID Enum
type instance ItemMeta   Enum = MiniMeta EnumID Enum
type instance StoreData  Enum = Enum
type instance NewData    Enum = NewEnum
type instance UpdateData Enum = UpdateEnum
type instance ViewData   Enum = Enum
type instance ItemData   Enum = ItemEnum
type instance Plural     Enum = "enums"
type instance Singular   Enum = "enum"

instance ToParamSchema EnumID
instance ToSchema EnumID     where declareNamedSchema = fmap unname . genericDeclareNamedSchema (prefixSchemaOptions [])
instance ToSchema Enum       where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions ["enum"]
instance ToSchema NewEnum    where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions ["ne"]
instance ToSchema UpdateEnum where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions ["ue"]
instance ToSchema ItemEnum   where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions ["ie"]

instance HasID Enum NewEnum where
  _ID = neID

instance HasID Enum EnumID where
  _ID = id

instance HasUpdate Enum where
  updateData _ (UpdateEnum n) = enumName .~ n

instance HasNew Enum where
  newData _ (NewEnum _ n) = Enum n nil

instance HasView Enum

instance HasItem Enum where
  itemData _ e = ItemEnum
    { _ieName = e ^. enumName
    , _ieOptionCount = e ^. optionCount
    }

-- Deletion is final but not exposed in the UI
instance HasDelete Enum

deriveJSON (prefixOptions ["opt"])  ''Option
deriveJSON (prefixOptions ["no"])   ''NewOption
deriveJSON (prefixOptions ["uo"])   ''UpdateOption
deriveJSON (prefixOptions ["enum"]) ''Enum
deriveJSON (prefixOptions ["ie"])   ''ItemEnum
deriveJSON (prefixOptions ["ne"])   ''NewEnum
deriveJSON (prefixOptions ["ue"])   ''UpdateEnum
