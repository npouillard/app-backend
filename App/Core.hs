{- Copyright (C) Nicolas Pouillard - All Rights Reserved
   Refer to the LICENSE file.
-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE Rank2Types                 #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

{-# OPTIONS -fno-warn-orphans           #-}

-- try again with lens >= 4.15.1
{-# OPTIONS -fno-warn-redundant-constraints #-}
module App.Core
  ( module App.Admin
  , module App.Common
  , module App.Core
  , module App.Person
  ) where
import qualified Control.Lens as L ((.=))
import Control.Monad.State (MonadState)
import Data.Aeson
import Data.Aeson.TH hiding (Options)
import Data.HashSet.Lens (setOf)
import Data.Time.Clock
import Data.Time.Calendar
import Prelude hiding (Enum)
import Web.HttpApiData (ToHttpApiData(toUrlPiece))

import App.Admin
import App.Common hiding ((.=))
import App.Person
import App.Enum

-- FIXME: Keep in sync with App.Server.Stack
data AnyEmail
  = PersonEmail (Email Person)
  | AdminEmail  (Email Admin)

makePrisms ''AnyEmail

instance ToJSON AnyEmail where
  toJSON (PersonEmail e) = object ["kind" .= String "person", "email" .= e]
  toJSON (AdminEmail  e) = object ["kind" .= String "admin",  "email" .= e]

data DB = DB
  { _admins    :: !(IDMap Admin)
  , _people    :: !(IDMap Person)
  , _enums     :: !(IDMap Enum)
  , _nextID    :: !Integer
  , _version   :: !DbVersion
  }
  deriving (Show, Eq, Generic)

makePrisms ''DB
makeLenses ''DB
deriveJSON (prefixOptions []) ''DB

type GetDB a = Fold DB a

type instance DBOf Admin    = DB
type instance DBOf Person   = DB
type instance DBOf Enum     = DB
type instance DBOf Option   = DB

data DbField a where
  Admins    :: DbField (IDMap Admin)
  People    :: DbField (IDMap Person)
  Enums     :: DbField (IDMap Enum)
  Options   :: !EnumID -> DbField (IDMap Option)
  NextID    :: DbField Integer
  Version   :: DbField DbVersion

type DBMap k = DbField (IDMap k)

dbFieldLens :: DbField a -> Lens' DB a
dbFieldLens = \case
  Admins    -> admins
  People    -> people
  Enums     -> enums
  Options i -> enums . at i . non (emptyEnum i) . _data . enumOptions
  NextID    -> nextID
  Version   -> version
  where
    who = Username "!auto"
    epoch = Timestamp (UTCTime (ModifiedJulianDay 0) 0)
    emptyEnum i = Record (Meta i who epoch []) (Enum Nothing nil)

dbFieldPath :: DbField a -> [ST]
dbFieldPath = \case
  Admins    -> ["admins"]
  People    -> ["people"]
  Enums     -> ["enums"]
  Options i -> ["enums", toUrlPiece i, "options"]
  NextID    -> ["next_id"]
  Version   -> ["version"]

data Update where
  UpdateMap  :: (ToJSON (StoreMeta k), ToJSON (StoreData k), ToHttpApiData (IDOf k)) =>
                DBMap k -> IDOf k -> Maybe (StoreRecord k) -> Update
  IncrNextID :: Update
  SendEmail :: AnyEmail -> Update

makePrisms ''Update

runUpdate :: MonadState DB m => Update -> m ()
runUpdate (UpdateMap fld i mr) = dbFieldLens fld . at i L..= mr
runUpdate IncrNextID           = nextID += 1
runUpdate SendEmail{}          = pure ()

setPath :: [ST] -> Value -> Value
setPath path v = foldr (\x y -> object [x .= y]) v path

instance ToJSON Update where
  toJSON (UpdateMap fld i mr) =
    setPath (dbFieldPath fld ++ [toUrlPiece i, "$set"]) (toJSON mr)
  toJSON IncrNextID =
    object ["next_id" .= String "$incr"]
  toJSON (SendEmail e) =
    object ["send_email" .= e]

data LogEntry = LogEntry
  { _updates   :: !(Vector Update)
  , _dbVersion :: !DbVersion
  }

deriveToJSON (prefixOptions ["db"]) ''LogEntry

selectQuery :: Query -> GetDB (StoreRecord Person)
selectQuery = selectMQuery . Just

selectMQuery :: Maybe Query -> GetDB (StoreRecord Person)
selectMQuery q = folding go where
  go db =
    db ^.. people . each
         . selectPeople   (compQ queryAll)
         . unselectPeople (compQ queryNot)
    where
      compQ l = q ^? _Just . l . _Just

instance HasSingleIDMap Admin    where singleIdMap _ = admins
instance HasSingleIDMap Person   where singleIdMap _ = people
