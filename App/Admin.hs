{- Copyright (C) Nicolas Pouillard - All Rights Reserved
   Refer to the LICENSE file.
-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE OverloadedStrings          #-}

-- try again with lens >= 4.15.1
{-# OPTIONS -fno-warn-redundant-constraints #-}
module App.Admin where
import Control.Lens
import Data.Aeson.TH
import Data.Swagger.Internal.Schema (ToSchema(..), genericDeclareNamedSchema)
import System.IO.Unsafe (unsafePerformIO)

import App.Common

data MetaAdmin = MetaAdmin
  { _maID        :: !(ID Admin)
  , _maCreatedBy :: !Username
  , _maCreatedAt :: !Timestamp
  , _maChanges   :: ![Change Admin]
  , _maNonce     :: !Nonce
  }
  deriving (Show, Eq, Ord, Generic)

data Admin = Admin
  { _adminLogin          :: !Login
  , _adminEmail          :: !EmailAddress
  , _adminHashedPassword :: !HashedPassword
  }
  deriving (Show, Eq, Ord, Generic)

data UpdateAdmin = UpdateAdmin
  { _uaLogin    :: !Login
  , _uaEmail    :: !EmailAddress
  , _uaPassword :: !(Maybe Password)
  }
  deriving (Show, Eq, Ord, Generic)

data ViewAdmin = ViewAdmin
  { _vaLogin :: !Login
  , _vaEmail :: !EmailAddress
  }
  deriving (Show, Eq, Ord, Generic)

deriveJSON (prefixOptions ["admin"]) ''Admin
deriveJSON (prefixOptions ["ma"])    ''MetaAdmin
deriveJSON (prefixOptions ["ua"])    ''UpdateAdmin
deriveJSON (prefixOptions ["va"])    ''ViewAdmin

makePrisms ''UpdateAdmin
makePrisms ''Admin
makeLenses ''UpdateAdmin
makeLenses ''Admin
makeLenses ''MetaAdmin

type instance IDOf       Admin = ID Admin
type instance StoreMeta  Admin = MetaAdmin
type instance ViewMeta   Admin = Meta (ID Admin) Admin
type instance ItemMeta   Admin = MiniMeta (ID Admin) Admin
type instance StoreData  Admin = Admin
type instance NewData    Admin = UpdateAdmin
type instance UpdateData Admin = UpdateAdmin
type instance ViewData   Admin = ViewAdmin
type instance ItemData   Admin = ViewAdmin
type instance Plural     Admin = "admins"
type instance Singular   Admin = "admin"

updateAdmin :: UpdateAdmin -> HashedPassword -> Admin
updateAdmin up old_password = Admin
  { _adminLogin = up ^. uaLogin
  , _adminEmail = up ^. uaEmail
  , _adminHashedPassword =
      case up ^. uaPassword of
        Nothing -> old_password
        Just p  -> unsafePerformIO $ hashPassword p -- Yes I'm ashamed!
  }

instance HasNew Admin where
  newMeta _ _ r = MetaAdmin
    { _maID        = m ^. metaID
    , _maCreatedBy = m ^. metaCreatedBy
    , _maCreatedAt = m ^. metaCreatedAt
    , _maChanges   = m ^. metaChanges
    , _maNonce     = 0
    } where m = r ^. _meta
  newData _ up = updateAdmin up nullPassword
    where
      nullPassword = HashedPassword ""

instance HasItem Admin

instance HasHashedPassword Admin where
  hashedPassword = adminHashedPassword

instance HasUpdate Admin where
  updateData _ up old = updateAdmin up $ old ^. hashedPassword

adminP :: Proxy Admin
adminP = Proxy

instance HasView Admin where
  viewMeta _ m _ = like Meta
    { _metaID        = m ^. maID
    , _metaCreatedBy = m ^. maCreatedBy
    , _metaCreatedAt = m ^. maCreatedAt
    , _metaChanges   = m ^. maChanges
    }
  viewData _ a = ViewAdmin { _vaLogin = a ^. login, _vaEmail = a ^. emailAddress }

-- Deletion is final
instance HasDelete Admin

instance HasID             Admin       MetaAdmin   where _ID            = maID
instance HasChanges        Admin       MetaAdmin   where changes _      = maChanges
instance HasCreatedBy      Admin       MetaAdmin   where createdBy _    = maCreatedBy
instance HasCreatedAt      Admin       MetaAdmin   where createdAt _    = maCreatedAt
instance HasLogin          UpdateAdmin             where login          = uaLogin
instance HasLogin          Admin                   where login          = adminLogin
instance HasUsername       Admin                   where username       = login . username
instance HasUsername       UpdateAdmin             where username       = login . username
instance HasEmailAddress   Admin                   where emailAddress   = adminEmail
instance HasEmailAddress   UpdateAdmin             where emailAddress   = uaEmail
instance HasNonce          MetaAdmin               where nonce          = maNonce

instance ToSchema Admin          where declareNamedSchema = error "Admin records contain sensitive information and should not be serialized"
instance ToSchema MetaAdmin      where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions ["ma"]
instance ToSchema ViewAdmin      where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions ["va"]
instance ToSchema UpdateAdmin    where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions ["ua"]
