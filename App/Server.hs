{- Copyright (C) Nicolas Pouillard - All Rights Reserved
   Refer to the LICENSE file.
-}
{-# LANGUAGE ConstraintKinds   #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE Rank2Types        #-}
{-# LANGUAGE TypeFamilies      #-}
module App.Server where

import App.Core
import App.Api
-- import App.Email
import App.Persist
import Control.Monad.Except (throwError)
import Data.Time.Calendar (toGregorian)
import Numeric.Lens
import Servant.API
import Servant.Server
import Servant.Server.Internal.Before (before)
import qualified Data.Vector as V

serveNewSession :: MonadPersist m => AuthSeed -> ServerT NewSession m
serveNewSession seed ld = do
  r <- findAdminByLogin $ ld ^. login
  if verifyPassword (ld ^. ldPassword) (r ^. _data . hashedPassword)
    then pure Session { _sessToken = authToken seed r
                      , _sessUsername = r ^. _data . login . username
                      , _sessID = r ^. _meta . _ID
                      }
    else throwError $ err403 { errBody = "Invalid password." }

serveDelSession :: Monad m => ID Admin -> ServerT (Del (Session Admin)) m
serveDelSession _ = pure () -- FIXME delete sessions [session] [prod]: actually this should bump the nonce!

serveSession :: MonadPersist m => AuthSeed -> ServerT SessionApi m
serveSession seed
    =  serveNewSession seed
  :<|> serveDelSession

type IsResource   k = (DBOf k ~ DB, HasUpdate k, HasView k, HasChanges k (StoreMeta k), HasDelete k,
                       ToHttpApiData (IDOf k), ToJSON (StoreData k), ToJSON (StoreMeta k))
type IsCollection k = (IsResource k, HasItem k, HasNew k, HasFreshID k)
type IsViewResource   k = (DBOf k ~ DB, HasView k, ToHttpApiData (IDOf k))
type IsViewCollection k = (IsViewResource k, HasItem k)

viewRecordDB :: (IsViewResource k, MonadPersist m) => DBMap k -> IDOf k -> ServerT (View k) m
viewRecordDB fld i = findFirstDB (viewRecordID (dbFieldLens fld) i)

serveResource :: (IsResource k, MonadPersist m) => AuthEnv -> DBMap k -> IDOf k -> ServerT (Resource k) m
serveResource a fld i
    =  viewRecordDB      fld i
  :<|> modifyRecordDB  a fld i
  :<|> deleteRecordDB  a fld i
  :<|> restoreRecordDB a fld i

serveViewCollection :: (IsViewCollection k, MonadPersist m)
                    => DBMap k -> ServerT (ViewCollection k) m
serveViewCollection fld
    =  findAllDB (itemRecords (dbFieldLens fld))
  :<|> findAllDB (viewRecords (dbFieldLens fld))
  :<|> viewRecordDB fld

serveCollection :: (IsCollection k, MonadPersist m)
                => AuthEnv -> DBMap k -> ServerT (Collection k) m
serveCollection a fld
    =  findAllDB (itemRecords (dbFieldLens fld))
  :<|> findAllDB (viewRecords (dbFieldLens fld))
  :<|> importDB a fld
  :<|> addDB a fld
  :<|> serveResource a fld

searchApi :: MonadPersist m => Query -> ServerT SearchApi m
searchApi q
    =  findAllDB (folding (\db -> db ^.. selectQuery q . folding (\r -> db ^? itemRecord personP r)))
  :<|> findAllDB (folding (\db -> db ^.. selectQuery q . folding (\r -> db ^? viewRecord personP r)))
  :<|> countDB (selectQuery q)
  :<|> averageDB foldBirthyear
  where
    foldBirthyear :: Fold DB Integer
    foldBirthyear = selectQuery q . _data . pBirthdate . _Just . _Birthdate . to toGregorian . _1

serveContribApi :: MonadPersist m => AuthEnv -> ID Person -> ServerT ContribApi m
serveContribApi who pid
    =  findFirstDB (viewRecordID people pid)
  :<|> modifyRecordDB   who People pid
  :<|> modifyRecordDB   who People pid
  :<|> validateRecordDB who People pid
  :<|> serveViewCollection . Options

adminApi :: MonadPersist m => AuthSeed -> Maybe (ID Admin) -> Maybe AuthToken -> ServerT AdminApi m
adminApi seed mid mtoken = before (Proxy :: Proxy AdminApi) (checkAuthToken seed mid mtoken) $ \(a, _admin) ->
       serveContribApi a
  :<|> serveCollection a Admins
  :<|> serveCollection a People
  :<|> serveCollection a Enums
  :<|> serveCollection a . Options
  :<|> searchApi

serveApi :: MonadPersist m => AuthSeed -> ServerT Api m
serveApi seed
    =  adminApi     seed
  :<|> serveContrib seed
  :<|> serveSession seed

serveContrib :: MonadPersist m => AuthSeed -> Maybe (ID Person) -> Maybe AuthToken -> ServerT ContribApi m
serveContrib seed mpid mtoken =
  before (Proxy :: Proxy ContribApi) (checkAuthToken seed mpid mtoken) $ \(a, p) ->
    serveContribApi a (p ^. _meta . _ID)
