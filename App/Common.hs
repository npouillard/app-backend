{- Copyright (C) Nicolas Pouillard - All Rights Reserved
   Refer to the LICENSE file.
-}
{-# LANGUAGE ConstraintKinds            #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE Rank2Types                 #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

-- try again with lens >= 4.15.1
{-# OPTIONS -fno-warn-redundant-constraints #-}
module App.Common
  ( module App.Boot
  , module App.Common
  , module App.Record
  , module Control.Lens
  , module Data.String
  , module Data.String.Conversions
  , module Debug.Trace
  , Generic
  , Proxy(Proxy)
  , Vector
  , ToJSON(toJSON)
  , FromJSON(parseJSON)
  , encode
  , decode
  ) where
import App.Boot
import App.Record
import Control.Lens
import Data.Aeson
import Data.Aeson.TH
import Data.Char
import Data.Digest.Pure.SHA (hmacSha256, showDigest)
import Data.List.Split
import Data.Maybe (fromMaybe)
import Data.Proxy (Proxy(Proxy))
import Data.String
import Data.String.Conversions
import Data.Swagger.Internal.Schema (ToSchema(..), genericDeclareNamedSchema,
                                     timeSchema, named, unname, passwordSchema, plain)
import Data.Swagger (ToParamSchema)
import Data.Swagger.Lens (pattern, minimum_, maximum_)
import Data.Time.Calendar (Day, toGregorian, fromGregorianValid)
import Data.Vector (Vector)
import Data.Word (Word32)
import Debug.Trace
import GHC.Generics (Generic)
import Web.HttpApiData (ToHttpApiData(toUrlPiece), FromHttpApiData)

import qualified Crypto.Scrypt as Scrypt
import qualified Data.Aeson.Types as JSON
import qualified Data.Text as ST

newtype NonBlank = UnsafeNonBlank { _nonBlank :: ST }
  deriving (Show, Eq, Ord, Generic, ToJSON)

makeLenses ''NonBlank

instance ToSchema NonBlank where
  declareNamedSchema = fmap nonBlankSchema . genericDeclareNamedSchema (prefixSchemaOptions [])
    where
      nonBlankSchema = pattern ?~ "^\\S(.*\\S)?$"

_NonBlank :: Iso' (Maybe NonBlank) ST
_NonBlank = iso (maybe "" _nonBlank) $ \x ->
  if isNonBlank x then Just $ UnsafeNonBlank x else Nothing

isNonBlank :: ST -> Bool
isNonBlank x = not (ST.null x) && x == ST.strip x

unsafeNonBlank :: String -> ST -> NonBlank
unsafeNonBlank msg s
  | isNonBlank s = UnsafeNonBlank s
  | otherwise    = error $ msg <> ": unsafeNonBlank of a blank string"

infixr 6 ?<>
infixr 6 <>?

(?<>) :: ST -> NonBlank -> NonBlank
x ?<> UnsafeNonBlank y = UnsafeNonBlank (x <> y)

(<>?) :: NonBlank -> ST -> NonBlank
UnsafeNonBlank x <>? y = UnsafeNonBlank (x <> y)

{-
instance IsString NonBlank where
  fromString = unsafeNonBlank "fromString" . fromString
-}

instance FromJSON NonBlank where
  parseJSON = \case
    String x | isNonBlank x -> pure $ UnsafeNonBlank x
    invalid  -> JSON.typeMismatch "Non-blank string" invalid

-- FIXME validate URL [validate] [prod]
newtype URL = URL { fromURL :: ST }
  deriving (Show, Eq, Ord, Generic, FromJSON, ToJSON)

-- FIXME validate EmailAddress [validate] [prod]
newtype EmailAddress = EmailAddress { fromEmailAddress :: ST }
  deriving (Show, Eq, Ord, Generic, FromJSON, ToJSON, FromHttpApiData, ToHttpApiData)

-- FIXME validate Password [validate] [prod]
newtype Password = Password { fromPassword :: ST }
  deriving (Show, Eq, Ord, Generic, FromJSON, ToJSON)

-- FIXME validate Login [validate] [prod]
newtype Login = Login { fromLogin :: ST }
  deriving (Show, Eq, Ord, Generic, FromJSON, ToJSON)

-- FIXME validate HashedPassword [validate] [prod]
newtype HashedPassword = HashedPassword { fromHashedPassword :: ST }
  deriving (Show, Eq, Ord, Generic, FromJSON, ToJSON)

verifyPassword :: Password -> HashedPassword -> Bool
verifyPassword (Password p) (HashedPassword h) =
  Scrypt.verifyPass' (Scrypt.Pass (cs p)) (Scrypt.EncryptedPass (cs h))

hashPassword :: Password -> IO HashedPassword
hashPassword = fmap (HashedPassword . cs . Scrypt.getEncryptedPass)
             . Scrypt.encryptPassIO' . Scrypt.Pass . cs . fromPassword

newtype Eventdate = Eventdate { fromEventdate :: Day }
  deriving (Show, Eq, Ord, Generic)

newtype Birthdate = Birthdate { fromBirthdate :: Day }
  deriving (Show, Eq, Ord, Generic)

newtype Count = Count { fromCount :: Int }
  deriving (Show, Eq, Ord, Generic, ToJSON)

newtype Days = Days { fromDays :: Int }
  deriving (Show, Eq, Ord, Generic, ToJSON)

checkPositiveNumber :: (Monad m, Num a, Ord a) => a -> m a
checkPositiveNumber n
  | n < 0     = fail "Negative number"
  | otherwise = pure n

instance FromJSON Days where
  parseJSON x = fmap Days . checkPositiveNumber =<< parseJSON x

instance FromJSON Count where
  parseJSON x = fmap Count . checkPositiveNumber =<< parseJSON x

instance ToSchema Days where
  declareNamedSchema = fmap (minimum_ ?~ 0) . genericDeclareNamedSchema (prefixSchemaOptions [])

instance ToSchema Count where
  declareNamedSchema = fmap (minimum_ ?~ 0) . genericDeclareNamedSchema (prefixSchemaOptions [])

newtype Year = Year { fromYear :: Int }
  deriving (Show, Eq, Ord, Generic, ToJSON)

-- Year should be in 1900..2100
instance FromJSON Year where
  parseJSON x = check =<< parseJSON x
    where
      check n | n < 1900  = fail "Unexpected year (< 1900)"
              | n > 2100  = fail "Unexpected year (> 2100)"
              | otherwise = pure $ Year n

instance ToSchema Year where
  declareNamedSchema = fmap ((minimum_ ?~ 1900) . (maximum_ ?~ 2100))
                     . genericDeclareNamedSchema (prefixSchemaOptions [])

-- FIXME validate AuthSeed [validate] [prod]
newtype AuthSeed = AuthSeed { fromAuthSeed :: ST }
  deriving (Show, Eq, Ord, Generic, FromJSON, ToJSON)

-- FIXME validate AuthToken [validate] [prod]
newtype AuthToken = AuthToken { fromAuthToken :: ST }
  deriving (Show, Eq, Ord, Generic, FromJSON, ToJSON, FromHttpApiData, ToHttpApiData)


--------------------------------------------------------------------------------
---- Nonce
--------------------------------------------------------------------------------

newtype Nonce = Nonce { fromNonce :: Word32 }
  deriving (Show, Eq, Ord, Generic, Num, FromJSON, ToJSON, FromHttpApiData, ToHttpApiData)

class HasNonce a where nonce :: Getter a Nonce

instance HasNonce m => HasNonce (Record m d) where nonce = _meta . nonce

instance ToSchema Nonce where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions []

makePrisms ''Nonce

data Session a = Session
  { _sessToken    :: !AuthToken
  , _sessUsername :: !Username
  , _sessID       :: !(ID a)
  }
  deriving (Show, Eq, Ord, Generic)

data LoginData = LoginData
  { _ldLogin    :: !Login
  , _ldPassword :: !Password
  }
  deriving (Show, Eq, Ord, Generic)

-- Country codes: FR, IT, DE...
-- ISO 3166-1 alpha-2
newtype CountryCode = CountryCode { fromCountryCode :: NonBlank }
  deriving (Show, Eq, Ord, Generic, FromJSON, ToJSON)

newtype Gender = Gender { fromGender :: NonBlank }
  deriving (Show, Eq, Ord, Generic, FromJSON, ToJSON)

-- Should not start with '@'
newtype Firstname = Firstname { fromFirstname :: NonBlank }
  deriving (Show, Eq, Ord, Generic, FromJSON, ToJSON)

newtype Lastname = Lastname { fromLastname :: NonBlank }
  deriving (Show, Eq, Ord, Generic, FromJSON, ToJSON)

newtype EmailSubject = EmailSubject { fromEmailSubject :: NonBlank }
  deriving (Show, Eq, Ord, Generic, FromJSON, ToJSON)

newtype EmailBody = EmailBody { fromEmailBody :: NonBlank }
  deriving (Show, Eq, Ord, Generic, FromJSON, ToJSON)

newtype EmailSignature = EmailSignature { fromEmailSignature :: NonBlank }
  deriving (Show, Eq, Ord, Generic, FromJSON, ToJSON)

data Comparator a
  = LessThan a
  | EqualTo  a
  | MoreThan a
  deriving (Show, Eq, Ord, Generic)

allComparators :: [a -> Comparator a]
allComparators = [LessThan,EqualTo,MoreThan]

--------------------------------------------------------------------------------
---- EmailDest
--------------------------------------------------------------------------------

data EmailDest k = EmailDest
  { _destAddress :: EmailAddress
  , _destID      :: ID k
  , _destNonce   :: Nonce
  }
  deriving (Show, Eq, Ord, Generic)

makeLenses ''EmailDest
deriveJSON (prefixOptions ["dest"]) ''EmailDest

instance ToSchema (EmailDest k) where declareNamedSchema = fmap unname . genericDeclareNamedSchema (prefixSchemaOptions ["dest"])
instance HasNonce (EmailDest k) where nonce = destNonce
instance IDOf k ~ ID k => HasID k  (EmailDest k) where _ID = destID


--------------------------------------------------------------------------------
---- Email
--------------------------------------------------------------------------------

data Email k = Email
  { _emailDests    :: !(Vector (EmailDest k))
  , _emailSubject  :: !EmailSubject
  , _emailBody     :: !EmailBody
  , _emailSign     :: !EmailSignature
  }
  deriving (Show, Eq, Ord, Generic)

makeLenses ''Email
deriveJSON (prefixOptions ["email"]) ''Email

instance ToSchema (Email k)      where declareNamedSchema = fmap unname . genericDeclareNamedSchema (prefixSchemaOptions ["email"])


--------------------------------------------------------------------------------
---- EmailMeta
--------------------------------------------------------------------------------

data EmailMeta = EmailMeta
  { _emailSentAt :: !Timestamp
  , _emailSentBy :: !Username
  }
  deriving (Show, Eq, Ord, Generic)

makeLenses ''EmailMeta
deriveJSON (prefixOptions ["email"]) ''EmailMeta

instance ToSchema EmailMeta where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions ["email"]


--------------------------------------------------------------------------------
---- Average
--------------------------------------------------------------------------------

data Average a = Average
  { _avSum   :: a
  , _avCount :: a
  }
  deriving (Show, Eq, Ord, Generic)

instance Num a => Monoid (Average a) where
  mempty = Average 0 0
  Average s0 c0 `mappend` Average s1 c1 = Average (s0 + s1) (c0 + c1)

pureAverage :: Num a => a -> Average a
pureAverage x = Average x 1

fractionalAverage :: Fractional a => Average a -> a
fractionalAverage (Average s c) = s / c

integralAverage :: Integral a => Average a -> a
integralAverage (Average s c) = s `div` c

averageOf :: Num a => Getting (Average a) s a -> s -> Average a
averageOf l = foldMapOf l pureAverage


--------------------------------------------------------------------------------
---- DbVersion
--------------------------------------------------------------------------------

newtype DbVersion = DbVersion { fromDbVersion :: Integer }
  deriving (Show, Eq, Ord, Generic, Enum, FromJSON, ToJSON, FromHttpApiData, ToHttpApiData)

data MailgunConfig = MailgunConfig
  { _mailgunDomain :: !ST
  , _mailgunApikey :: !ST
  , _mailgunSender :: !ST
 -- , _mailgunProxy  :: !(Maybe Proxy)
  }
  deriving (Show, Eq, Ord, Generic)

newtype Origin = Origin { fromOrigin :: ST }
  deriving (Show, Eq, Ord, Generic, FromJSON, ToJSON)

data CorsConfig = AnyOrigin | FromOrigin (Vector Origin)
  deriving (Show, Eq, Ord, Generic)

instance ToJSON CorsConfig where
  toJSON = \case
    AnyOrigin     -> String "*"
    FromOrigin os -> toJSON os

instance FromJSON CorsConfig where
  parseJSON = \case
    String "*" -> pure AnyOrigin
    v@Array{}  -> FromOrigin <$> parseJSON v
    invalid    -> JSON.typeMismatch "CorsConfig" invalid

data Config = Config
  { _confListenHost      :: !ST
  , _confListenPort      :: !Int
  , _confSaveRateSeconds :: !Int
  , _confOptimisticLock  :: !Bool
  , _confDbDumpJson      :: !FilePath
  , _confDbLogJson       :: !FilePath
  , _confMailgun         :: !MailgunConfig
  , _confCors            :: !CorsConfig
  , _confAuthSeed        :: !AuthSeed
  }
  deriving (Show, Eq, Ord, Generic)

deriveJSON (prefixOptions ["mailgun"]) ''MailgunConfig
deriveJSON (prefixOptions ["conf"]) ''Config
deriveJSON (prefixOptions ["ld"]) ''LoginData
deriveJSON (prefixOptions ["av"]) ''Average
deriveJSON (prefixOptions [""]) ''Comparator
deriveJSON (prefixOptions ["sess"]) ''Session

instance FromJSON Birthdate where parseJSON = fmap Birthdate . parseJSONDay
instance FromJSON Eventdate where parseJSON = fmap Eventdate . parseJSONDay
instance ToJSON Birthdate where toJSON = String . dayST . fromBirthdate
instance ToJSON Eventdate where toJSON = String . dayST . fromEventdate
instance ToSchema Birthdate where declareNamedSchema _ = pure $ named "Birthdate" (timeSchema "dd/mm/yyyy")
instance ToSchema Eventdate where declareNamedSchema _ = pure $ named "Eventdate" (timeSchema "dd/mm/yyyy")

parseJSONDay :: Monad m => Value -> m Day
parseJSONDay = \case
  String x ->
    case splitOn "/" (cs x) of
      [d,m,y]
        | length d == 2 && length y == 4 && length m == 2 &&
          all isDigit d && all isDigit y && all isDigit m
        -> maybe (fail "invalid date: invalid gregorian date") pure $
            fromGregorianValid (read y) (read m) (read d)
      _ -> fail "invalid date: format DD/MM/YYYY"
  _ -> fail "invalid date: format DD/MM/YYYY"

dayST :: Day -> ST
dayST date = pad 2 d <> "/" <> pad 2 m <> "/" <> pad 4 y
  where
    (y,m,d) = toGregorian date
    pad :: Show a => Int -> a -> ST
    pad n x = ST.replicate (n - ST.length s) "0" <> s
      where s = cs $ show x

instance ToParamSchema AuthToken

{-
instance ToSchema a =>
         ToSchema (NonEmpty a)   where declareNamedSchema _ = declareNamedSchema (Proxy :: Proxy [a])
-}
instance ToSchema Password       where declareNamedSchema _ = plain passwordSchema
instance ToSchema Login          where declareNamedSchema _ = declareNamedSchema (Proxy :: Proxy ST)

instance ToSchema AuthToken      where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions []
instance ToSchema CountryCode    where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions []
instance ToSchema EmailAddress   where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions []
instance ToSchema EmailBody      where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions []
instance ToSchema EmailSignature where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions []
instance ToSchema EmailSubject   where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions []
instance ToSchema Firstname      where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions []
instance ToSchema Lastname       where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions []
instance ToSchema Gender         where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions []

instance ToSchema a =>
         ToSchema (Comparator a) where declareNamedSchema = fmap unname . genericDeclareNamedSchema (prefixSchemaOptions [""])
instance ToSchema a =>
         ToSchema (Average a)    where declareNamedSchema = fmap unname . genericDeclareNamedSchema (prefixSchemaOptions ["av"])
instance ToSchema (Session a)    where declareNamedSchema = fmap unname . genericDeclareNamedSchema (prefixSchemaOptions ["sess"])

instance ToSchema LoginData      where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions ["ld"]

makePrisms ''NonBlank
makePrisms ''EmailAddress
makePrisms ''Password
makePrisms ''Login
makePrisms ''HashedPassword
makePrisms ''Birthdate
makePrisms ''AuthToken
makePrisms ''CountryCode
makePrisms ''Gender
makePrisms ''Firstname
makePrisms ''Lastname
makePrisms ''EmailSubject
makePrisms ''EmailBody
makePrisms ''EmailSignature
makePrisms ''LoginData
makePrisms ''Session
makePrisms ''Origin
makePrisms ''CorsConfig
makePrisms ''URL
makePrisms ''Days
makePrisms ''Count

makeLenses ''LoginData
makeLenses ''Session
makeLenses ''Config
makeLenses ''CorsConfig
makeLenses ''MailgunConfig
makePrisms ''Eventdate

instance IDOf k ~ ID k =>
         HasID k (Session k)   where _ID = sessID

class    HasLogin a            where login :: Lens' a Login
instance HasLogin LoginData    where login = ldLogin
instance HasLogin d =>
         HasLogin (Record m d) where login = _data . login

class    HasPassword a            where password :: Lens' a Password
instance HasPassword LoginData    where password = ldPassword
instance HasPassword d =>
         HasPassword (Record m d) where password = _data . password

class    HasHashedPassword a            where hashedPassword :: Lens' a HashedPassword
instance HasHashedPassword d =>
         HasHashedPassword (Record m d) where hashedPassword = _data . hashedPassword

instance HasUsername LoginData         where username = login . username
instance HasUsername Login             where username = to $ Username . ("@" <>) . fromLogin
instance HasUsername (Session a)       where username = sessUsername

class    HasEmailAddress a             where emailAddress :: Getter a EmailAddress
instance HasEmailAddress d =>
         HasEmailAddress (Record m d)  where emailAddress = _data . emailAddress
instance HasEmailAddress (EmailDest k) where emailAddress = destAddress

class    HasGender a                where gender :: Getter a Gender

class    HasFirstname a             where firstname :: Getter a Firstname

class    HasLastname  a             where lastname  :: Getter a Lastname

type HasAuthToken k a = (HasID k a, HasEmailAddress a, HasNonce a)

data AuthTokenData k = AuthTokenData
  { _atID    :: ID k
  , _atEmail :: EmailAddress
  , _atNonce :: Nonce
  }

makeLenses ''AuthTokenData

instance HasNonce        (AuthTokenData k) where nonce = atNonce
instance IDOf k ~ ID k =>
         HasID k         (AuthTokenData k) where _ID = atID
instance HasEmailAddress (AuthTokenData k) where emailAddress = atEmail

authToken :: (ToHttpApiData (IDOf k), HasAuthToken k a) => AuthSeed -> a -> AuthToken
authToken (AuthSeed seed) k = h $ ST.unwords [toUrlPiece i, toUrlPiece a, toUrlPiece n]
  where
    -- t x = trace (cs x) x
    h = AuthToken . cs . showDigest . hmacSha256 (cs seed) . cs -- . t
    i = k ^. _ID
    a = k ^. emailAddress
    n = k ^. nonce

-- Fullname should not start with '@'
fullname :: (HasFirstname a, HasLastname a, Monoid r) => Getting r a ST
fullname = firstname . _Firstname . nonBlank
        <> like " "
        <> lastname . _Lastname . nonBlank

emailDest :: (HasEmailAddress p, HasID k p, HasNonce p, IDOf k ~ ID k) => Getter p (EmailDest k)
emailDest = to $ \p -> EmailDest { _destAddress = p ^. emailAddress
                                 , _destID      = p ^. _ID
                                 , _destNonce   = p ^. nonce
                                 }

matchCmp :: Ord a => Comparator a -> a -> Bool
matchCmp (LessThan x) y = y <  x
matchCmp (EqualTo  x) y = y == x
matchCmp (MoreThan x) y = y >  x

matchWith :: (b -> b' -> Bool) -> a -> Fold a b -> a' -> Fold a' b' -> [Bool]
matchWith rel s f t g =
  case s ^.. f of
    [] -> []
    xs -> [or [ rel x y | x <- xs, y <- t ^.. g ]]

match :: Eq b => a -> Fold a b -> a' -> Fold a' b -> [Bool]
match = matchWith (==)

matchWithCmp :: Ord b => a -> Fold a (Comparator b) -> a' -> Fold a' b -> [Bool]
matchWithCmp = matchWith matchCmp

-- When the event is not set (equal to `Nothing`), then its date is considered
-- greater than any date.
matchEndDate :: a -> Fold a (Comparator Eventdate) -> a' -> Fold a' (Maybe e) -> Getter e Eventdate -> [Bool]
matchEndDate q f p g getEventDate = matchWith cmp q f p g
  where
    cmp opCmp        (Just y) = matchCmp opCmp (y ^. getEventDate)
    cmp (MoreThan _) Nothing  = True
    cmp (EqualTo  _) Nothing  = False
    cmp (LessThan _) Nothing  = False

(?|) :: Maybe a -> Endom a
(?|) = flip fromMaybe

nil :: Monoid m => m
nil = mempty

-- Right if its all-Right:     `traverseEithers (Right <$> xs) = Right xs`.
-- Left what's left otherwise: `traverseEithers [Left x, Right y, Left z] = Left [x, z]`.
traverseEithers :: [Either a b] -> Either [a] [b]
traverseEithers xs =
  case xs ^? below _Right of
    Just as -> Right as
    Nothing -> Left $ xs ^.. each . _Left
