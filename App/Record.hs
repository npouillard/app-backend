{- Copyright (C) Nicolas Pouillard - All Rights Reserved
   Refer to the LICENSE file.
-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE Rank2Types                 #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE FunctionalDependencies     #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE UndecidableInstances       #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DefaultSignatures          #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE ConstraintKinds            #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE OverloadedStrings          #-}

-- try again with lens >= 4.15.1
{-# OPTIONS -fno-warn-redundant-constraints #-}
module App.Record where
import App.Boot
import Control.Lens
import Data.Aeson
import Data.Aeson.TH
import Data.Char
import Data.Hashable (Hashable(hash,hashWithSalt))
import Data.HashMap.Strict (HashMap)
import Data.Proxy (Proxy(Proxy))
import Data.String.Conversions
import Data.Swagger.Internal.Schema (ToSchema(..), genericDeclareNamedSchema,
                                     timeSchema, named, unname)
import Data.Swagger (ToParamSchema)
import Data.Time.Clock (UTCTime)
import Data.Time.Format (parseTimeM, formatTime, defaultTimeLocale)
import GHC.Generics (Generic)
import GHC.TypeLits (Symbol)
import Web.HttpApiData (ToHttpApiData(toUrlPiece), FromHttpApiData)

import qualified Data.Aeson.Types as JSON
import qualified Data.Text as ST

with :: proxy k -> Proxy k
with = const Proxy

-- The surrounding database
type family DBOf k

type GetDBOf  k f = Fold  (DBOf k) (f k)
type LensDBOf k f = Lens' (DBOf k) (f k)

-- What the identifiers are
-- (ToHttpApiData (IDOf k) is used as the key in IDMap)
type family IDOf k

-- What gets stored in the DB
type family StoreMeta k
type family StoreData k

-- What gets presented to the user
type family ViewMeta k
type family ViewData k

-- What gets returned from the user to add a record to the DB
type family NewData k

-- What gets returned from the user to update the DB
type family UpdateData k

-- What gets presented to the user when viewing a list of items
type family ItemMeta k
type family ItemData k

-- type instance Plural  Admin = "admins"
-- type instance Singlar Admin = "admin"
type family Plural   k :: Symbol
type family Singular k :: Symbol

data Record m d = Record
  { __meta :: !m
  , __data :: !d }
  deriving (Generic, Show, Eq, Ord)

instance (ToSchema m, ToSchema d) =>
         ToSchema (Record m d)   where declareNamedSchema = fmap unname . genericDeclareNamedSchema (prefixSchemaOptions [""])

makePrisms ''Record
makeLenses ''Record

type StoreRecord k = Record (StoreMeta k) (StoreData k)

type ItemRecord k = Record (ItemMeta k) (ItemData k)

type ViewRecord k = Record (ViewMeta k) (ViewData k)

deriveJSON (prefixOptions [""]) ''Record


--------------------------------------------------------------------------------
---- ID
--------------------------------------------------------------------------------

newtype ID a = ID { fromID :: Integer }
  deriving (Show, Eq, Ord, Generic, ToJSON, FromHttpApiData, ToHttpApiData)

type instance IDOf (ID k) = ID k

mkID :: proxy a -> Integer -> ID a
mkID _ = ID

voidID :: ID a -> ID ()
voidID (ID i) = (ID i)

instance Hashable (ID a) where
  hashWithSalt s (ID i) = hashWithSalt s i
  hash (ID i) = hash i

-- An ID is an integer however the parser is a bit more lenient and also
-- accepts strings which represents an integer.
instance FromJSON (ID a) where
  parseJSON = \case
    Number x             -> ID <$> parseJSON (Number x)
    String x
      | ST.all isDigit x
     && not (ST.null x)  -> pure . ID . read $ cs x
    invalid              -> JSON.typeMismatch "ID" invalid

instance ToParamSchema (ID a)
instance ToSchema (ID a) where declareNamedSchema = fmap unname . genericDeclareNamedSchema (prefixSchemaOptions [])

class HasID k a | a -> k                   where _ID :: Lens' a (IDOf k)
instance IDOf k ~ ID k => HasID k (ID k)   where _ID = id
instance HasID k m => HasID k (Record m d) where _ID = _meta . _ID


--------------------------------------------------------------------------------
--- Username
--------------------------------------------------------------------------------

-- Either an admin login (starts with @ and has no spaces?) or a person full name
-- FIXME validate Username [validate] [prod]
-- SHOULD not have spaces
newtype Username = Username { fromUsername :: ST }
  deriving (Show, Eq, Ord, Generic, FromJSON, ToJSON)

class    HasUsername a                 where username :: Getter a Username
instance HasUsername Username          where username = id
instance HasUsername a =>
         HasUsername (Record m a)      where username = _data . username

instance ToSchema Username       where declareNamedSchema _ = declareNamedSchema (Proxy :: Proxy ST)

makePrisms ''Username


--------------------------------------------------------------------------------
--- Timestamp
--------------------------------------------------------------------------------

newtype Timestamp = Timestamp { fromTimestamp :: UTCTime }
  deriving (Show, Eq, Ord, Generic)

timestampFormat :: String
timestampFormat = "%Y-%m-%dT%H:%M:%SZ%q"
timestampSchema :: ST
timestampSchema = "yyyy-mm-ddThh:MM:ssZq"

instance FromJSON Timestamp where
  parseJSON = \case
    String x -> Timestamp <$> parseTimeM False defaultTimeLocale timestampFormat (cs x)
    invalid  -> JSON.typeMismatch "Timestamp" invalid

instance ToJSON Timestamp where
  toJSON = toJSON . formatTime defaultTimeLocale timestampFormat . fromTimestamp

instance ToSchema Timestamp where
  declareNamedSchema _ = pure $ named "Timestamp" (timeSchema timestampSchema)

makePrisms ''Timestamp
makeLenses ''Timestamp


--------------------------------------------------------------------------------
---- Change & ChangeType
--------------------------------------------------------------------------------

data ChangeType = Update | Delete | Restore | Notification !(ID ()) | Validation
  deriving (Show, Eq, Ord, Generic)

data Change k = Change
  { _changeBy   :: !Username
  , _changeAt   :: !Timestamp
  , _changeType :: !ChangeType
  }
  deriving (Show, Eq, Ord, Generic)

instance ToSchema (Change a)     where declareNamedSchema = fmap unname . genericDeclareNamedSchema (prefixSchemaOptions ["change"])
instance ToSchema ChangeType     where declareNamedSchema = genericDeclareNamedSchema $ prefixSchemaOptions []

class    HasChanges k a | a -> k where changes :: proxy k -> Lens' a [Change k]

class    HasCreatedBy k a | a -> k where createdBy :: proxy k -> Lens' a Username

class    HasCreatedAt k a | a -> k where createdAt :: proxy k -> Lens' a Timestamp

makeLenses ''Change
deriveJSON (prefixOptions ["change"]) ''Change
deriveJSON (prefixOptions [""]) ''ChangeType


--------------------------------------------------------------------------------
---- Meta & MiniMeta
--------------------------------------------------------------------------------

data Meta i k = Meta
  { _metaID        :: !i
  , _metaCreatedBy :: !Username
  , _metaCreatedAt :: !Timestamp
  , _metaChanges   :: ![Change k]
  -- ^ ordered by date of change, most recent change first.
  }
  deriving (Show, Eq, Ord, Generic)

data MiniMeta i k = MiniMeta
  { _mmID        :: !i
  , _mmChangedAt :: !Timestamp
  , _mmDeletedAt :: !(Maybe Timestamp)
  }
  deriving (Show, Eq, Ord, Generic)

makePrisms ''Meta
makeLenses ''Meta
makeLenses ''MiniMeta
deriveJSON (prefixOptions ["meta"]) ''Meta
deriveJSON (prefixOptions ["mm"]) ''MiniMeta

type NewRecord k = Record (Meta (IDOf k) k) (NewData k)

type HasMeta k a = (HasID k a, HasChanges k a, HasCreatedAt k a)

instance ToSchema i =>
         ToSchema (Meta i a)     where declareNamedSchema = fmap unname . genericDeclareNamedSchema (prefixSchemaOptions ["meta"])
instance ToSchema i =>
         ToSchema (MiniMeta i a) where declareNamedSchema = fmap unname . genericDeclareNamedSchema (prefixSchemaOptions ["mm"])

instance HasID k i =>
         HasID        k (Meta i k) where _ID = metaID . _ID
instance HasChanges   k (Meta i k) where changes _ = metaChanges
instance HasCreatedBy k (Meta i k) where createdBy _ = metaCreatedBy
instance HasCreatedAt k (Meta i k) where createdAt _ = metaCreatedAt

changedAt :: (HasCreatedAt k a, HasChanges k a, Monoid r)
          => proxy k -> Getting r a Timestamp
changedAt p = changes p . _head . changeAt <> createdAt p

-- This function considers that a record is deleted if it has been deleted at
-- some point.
  -- TODO
deletedAt :: (HasChanges k a, Monoid r)
          => proxy k -> Getting r a Timestamp
deletedAt p = folding f
  where
    changeTypeElem types = each . filtered ((`elem` types) . view changeType)
    f r = r ^? changes p . changeTypeElem [Delete, Restore]
            ^? changeTypeElem [Delete] . changeAt

miniMeta :: forall k m. HasMeta k m => m -> MiniMeta (IDOf k) k
miniMeta m = MiniMeta
  { _mmID        = m ^. _ID
  , _mmChangedAt = m ^?! changedAt (Proxy :: Proxy k)
  , _mmDeletedAt = m ^? deletedAt (Proxy :: Proxy k)
  }

--------------------------------------------------------------------------------
---- IDMap
--------------------------------------------------------------------------------

newtype IDMap k = IDMap { fromIMap :: HashMap ST (StoreRecord k) }
  deriving (Generic)

deriving instance (Show (StoreMeta k), Show (StoreData k)) => Show (IDMap k)
deriving instance (Eq (StoreMeta k), Eq (StoreData k)) => Eq (IDMap k)
deriving instance (FromJSON (StoreMeta k), FromJSON (StoreData k)) => FromJSON (IDMap k)
deriving instance (ToJSON (StoreMeta k), ToJSON (StoreData k)) => ToJSON (IDMap k)

instance Monoid (IDMap a) where
  mempty = IDMap mempty
  mappend (IDMap m0) (IDMap m1) = IDMap (mappend m0 m1)

type instance Index   (IDMap a) = IDOf a
type instance IxValue (IDMap a) = StoreRecord a

instance ToHttpApiData (IDOf k) =>
         At (IDMap k) where
  at i f (IDMap m) = IDMap <$> at (toUrlPiece i) f m

instance ToHttpApiData (IDOf k) =>
         Ixed (IDMap k) where
  ix i f (IDMap m) = IDMap <$> ix (toUrlPiece i) f m

instance (StoreMeta a ~ ma, StoreData a ~ da, StoreMeta b ~ mb, StoreData b ~ db) =>
         Each (IDMap a) (IDMap b) (Record ma da) (Record mb db) where
  each f (IDMap m) = IDMap <$> each f m

instance (ToSchema (StoreMeta k), ToSchema (StoreData k)) => ToSchema (IDMap k) where
  declareNamedSchema = fmap unname . genericDeclareNamedSchema (prefixSchemaOptions [])

class ToHttpApiData (IDOf k) => HasSingleIDMap k where
  singleIdMap :: proxy k -> LensDBOf k IDMap

--------------------------------------------------------------------------------
---- HasView
--------------------------------------------------------------------------------

class HasView k where
  viewMeta :: proxy k -> StoreMeta k -> StoreData k -> GetDBOf k ViewMeta
  viewData :: proxy k -> StoreData k ->                           ViewData k

  default viewMeta :: (StoreMeta k ~ ViewMeta k)
                   => proxy k -> StoreMeta k -> StoreData k -> GetDBOf k ViewMeta
  viewMeta _ m _ = like m

  default viewData :: (StoreData k ~ ViewData k) => proxy k -> StoreData k -> ViewData k
  viewData _ = id

  viewRecord :: proxy k -> StoreRecord k -> GetDBOf k ViewRecord
  viewRecord proxy (Record m d) = viewMeta proxy m d . to (`Record` viewData proxy d)


--------------------------------------------------------------------------------
---- HasItem
--------------------------------------------------------------------------------

class HasItem k where
  itemMeta :: proxy k -> StoreMeta k -> StoreData k -> GetDBOf k ItemMeta
  itemData :: proxy k -> StoreData k ->                           ItemData k

  default itemMeta :: (HasMeta k (StoreMeta k), ItemMeta k ~ MiniMeta (IDOf k) k) =>
                      proxy k -> StoreMeta k -> StoreData k -> GetDBOf k ItemMeta
  itemMeta _ m _ = like $ miniMeta m

  default itemData :: (HasView k, ViewData k ~ ItemData k) => proxy k -> StoreData k -> ItemData k
  itemData = viewData

  itemRecord :: proxy k -> StoreRecord k -> GetDBOf k ItemRecord
  itemRecord proxy (Record m d) = itemMeta proxy m d . to (`Record` itemData proxy d)


--------------------------------------------------------------------------------
---- HasNew
--------------------------------------------------------------------------------

class HasNew k where
  newMeta :: proxy k -> DBOf k -> NewRecord  k -> StoreMeta k
  newData :: proxy k ->           NewData    k -> StoreData k

  default newMeta :: Meta (IDOf k) k ~ StoreMeta k
                  => proxy k -> DBOf k -> NewRecord k -> StoreMeta k
  newMeta _ _ r = r ^. _meta

  default newData :: NewData k ~ StoreData k => proxy k -> NewData k -> StoreData k
  newData _ = id

  newRecord :: proxy k -> DBOf k -> NewRecord k -> StoreRecord k
  newRecord proxy db r = Record (newMeta proxy db r) (newData proxy (r ^. _data))


--------------------------------------------------------------------------------
---- HasUpdate
--------------------------------------------------------------------------------

class HasUpdate k where
  updateMeta :: proxy k -> UpdateData k -> DBOf k -> Endom (StoreMeta k)
  updateMeta _ _ _ = id
  updateData :: proxy k -> UpdateData k ->           Endom (StoreData k)

  default updateData :: (HasNew k, NewData k ~ UpdateData k) => proxy k -> UpdateData k -> Endom (StoreData k)
  updateData p up _ = newData p up

  updateRecord :: proxy k -> UpdateData k -> DBOf k -> Endom (StoreRecord k)
  updateRecord proxy up db = (_meta %~ updateMeta proxy up db)
                           . (_data %~ updateData proxy up)


--------------------------------------------------------------------------------
---- HasDelete
--------------------------------------------------------------------------------

class HasDelete k where
  deleteRecord :: proxy k -> DBOf k -> StoreRecord k -> Maybe (StoreRecord k)
  deleteRecord _ _ _ = Nothing

--------------------------------------------------------------------------------

pureViewData :: (s -> a) -> s -> Fold db a
pureViewData f s = to (const $ f s)

pureViewData' :: Getter s a -> s -> Fold db a
pureViewData' f s = to (const $ s ^. f)

proxyGetDBOfIDMap :: GetDBOf k IDMap -> Proxy k
proxyGetDBOfIDMap _ = Proxy

itemRecordID :: (ToHttpApiData (IDOf k), HasItem k) => GetDBOf k IDMap -> IDOf k -> GetDBOf k ItemRecord
itemRecordID idMap i = folding $ \db -> do s <- db ^? idMap . at i . _Just
                                           db ^? itemRecord (proxyGetDBOfIDMap idMap) s

viewRecordID :: (HasView k, ToHttpApiData (IDOf k)) => GetDBOf k IDMap -> IDOf k -> GetDBOf k ViewRecord
viewRecordID idMap i = folding $ \db -> do s <- db ^? idMap . at i . _Just
                                           db ^? viewRecord (proxyGetDBOfIDMap idMap) s

deepRecords :: ToHttpApiData (IDOf k) => GetDBOf k IDMap -> (StoreRecord k -> Fold (DBOf k) a) -> Fold (DBOf k) a
deepRecords idMap f = folding $ \db -> db ^.. idMap . each . folding ((^?) db . f)

storeRecords :: ToHttpApiData (IDOf k) => GetDBOf k IDMap -> GetDBOf k StoreRecord
storeRecords idMap = deepRecords idMap like

viewRecords :: (ToHttpApiData (IDOf k), HasView k) => GetDBOf k IDMap -> GetDBOf k ViewRecord
viewRecords idMap = deepRecords idMap (viewRecord (proxyGetDBOfIDMap idMap))

itemRecords :: (ToHttpApiData (IDOf k), HasItem k) => GetDBOf k IDMap -> GetDBOf k ItemRecord
itemRecords idMap = deepRecords idMap (itemRecord (proxyGetDBOfIDMap idMap))
