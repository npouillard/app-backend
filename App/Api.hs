{- Copyright (C) Nicolas Pouillard - All Rights Reserved
   Refer to the LICENSE file.
-}
{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE TypeFamilies       #-}
{-# LANGUAGE TypeOperators      #-}
module App.Api (module App.Api, module App.Api.Core) where

import Prelude hiding (Enum)
import App.Api.Core
import App.Enum
import App.Core hiding (Version)
import Data.Swagger as S hiding (Header)
import Servant.API
import Servant.Swagger

type NewSession = BodyJ LoginData :> NewJ (Session Admin)
type SessionApi
    =  NewSession
  :<|> CaptureID Admin :> Del (Session Admin)
-- "reset" :> VoidJ

type ContribApi
    =  GetJ  (ViewRecord Person)
  :<|> BodyJ (UpdateData Person) :> PutJ  (ViewRecord Person)
  :<|> BodyJ (UpdateData Person) :> PostJ (ViewRecord Person)
  -- Since old browsers do not support PUT, we accept POST as well.
  :<|> "commit" :> PostJ (ViewRecord Person)
  :<|> "enum" :> CaptureID Enum :> ViewCollection Option

type SearchApi
    =  "search"  :> PostJ [ItemRecord Person]
  :<|> "export"  :> PostJ [ViewRecord Person]
  :<|> "count"   :> PostJ Count
  :<|> "average" :> "birthyear" :> PostJ (Average Integer)

type AdminApi
    =  "person" :> CaptureID Person :> "contrib" :> ContribApi
  :<|> Collection Admin
  :<|> Collection Person
  :<|> Collection Enum
  :<|> "enum" :> CaptureID Enum :> Collection Option
  :<|> BodyJ Query :> SearchApi

type XAuthToken  = "X-Auth-Token"
type XAuthId     = "X-Auth-Id"
type AuthHeaders k a = Header XAuthId (IDOf k) :> Header XAuthToken AuthToken :> a

type Api
    =  AuthHeaders Admin  AdminApi
  :<|> "contrib" :> AuthHeaders Person ContribApi
  :<|> "session" :> SessionApi

api :: Proxy Api
api = Proxy

swaggerApi :: Swagger
swaggerApi = toSwagger api
  -- & S.host        ?~ "FIXME"
  & S.info . S.title   .~ "TODO"
  & S.info . S.version .~ "v0.1"
