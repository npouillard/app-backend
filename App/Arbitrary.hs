{- Copyright (C) Nicolas Pouillard - All Rights Reserved
   Refer to the LICENSE file.
-}
{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies        #-}

{-# OPTIONS_GHC -fno-warn-orphans -Werror #-}

module App.Arbitrary
    ( loremIpsum
    , Gen
    , generate
    , arbitrary
    , arb, arb1, arb2, arb3, arb4, arb5, arb6, arb7, arb8, arb9, arb10
    , arbWord, arbPhrase, arbParagraph
    , arbHex, arbMaybe, arbMaybe'
    , addDBArb, populateDB
    , devRootUpdateAdmin
    ) where

import App.Core hiding (elements)
import App.Persist
import Control.Monad
import Control.Monad.IO.Class
import Data.Char
import Data.List
import Data.Time (fromGregorian)
import Test.QuickCheck (Arbitrary(..), Gen, elements, oneof, generate, arbitrary)
import Test.QuickCheck.Instances ()
import System.IO.Unsafe (unsafePerformIO)

import qualified Data.Text as ST
import qualified Data.Vector as V


----------------------------------------------------------------------
-- general-purpose helpers

arb :: Arbitrary a => Gen a
arb = arbitrary

arb1 :: Arbitrary a => (a -> b) -> Gen b
arb1 f = f <$> arb

arb2 :: (Arbitrary a, Arbitrary b) => (a -> b -> c) -> Gen c
arb2 f = f <$> arb <*> arb

arb3 :: (Arbitrary a, Arbitrary b, Arbitrary c) => (a -> b -> c -> d) -> Gen d
arb3 f = f <$> arb <*> arb <*> arb

arb4 :: (Arbitrary a, Arbitrary b, Arbitrary c, Arbitrary d) => (a -> b -> c -> d -> e) -> Gen e
arb4 f = f <$> arb <*> arb <*> arb <*> arb

arb5 :: (Arbitrary a, Arbitrary b, Arbitrary c, Arbitrary d, Arbitrary e) =>
        (a -> b -> c -> d -> e -> f) -> Gen f
arb5 f = f <$> arb <*> arb <*> arb <*> arb <*> arb

arb6 :: (Arbitrary a, Arbitrary b, Arbitrary c, Arbitrary d, Arbitrary e, Arbitrary f) =>
        (a -> b -> c -> d -> e -> f -> g) -> Gen g
arb6 f = f <$> arb <*> arb <*> arb <*> arb <*> arb <*> arb

arb7 :: (Arbitrary a, Arbitrary b, Arbitrary c, Arbitrary d, Arbitrary e, Arbitrary f
        ,Arbitrary g) =>
        (a -> b -> c -> d -> e -> f -> g -> h) -> Gen h
arb7 f = f <$> arb <*> arb <*> arb <*> arb <*> arb <*> arb <*> arb

arb8 :: (Arbitrary a, Arbitrary b, Arbitrary c, Arbitrary d, Arbitrary e, Arbitrary f
        ,Arbitrary g, Arbitrary h) =>
        (a -> b -> c -> d -> e -> f -> g -> h -> i) -> Gen i
arb8 f = f <$> arb <*> arb <*> arb <*> arb <*> arb <*> arb <*> arb <*> arb

arb9 :: (Arbitrary a, Arbitrary b, Arbitrary c, Arbitrary d, Arbitrary e, Arbitrary f
        ,Arbitrary g, Arbitrary h, Arbitrary i) =>
        (a -> b -> c -> d -> e -> f -> g -> h -> i -> j) -> Gen j
arb9 f = f <$> arb <*> arb <*> arb <*> arb <*> arb <*> arb <*> arb <*> arb <*> arb

arb10 :: (Arbitrary a, Arbitrary b, Arbitrary c, Arbitrary d, Arbitrary e, Arbitrary f
         ,Arbitrary g, Arbitrary h, Arbitrary i, Arbitrary j) =>
         (a -> b -> c -> d -> e -> f -> g -> h -> i -> j -> k) -> Gen k
arb10 f = f <$> arb <*> arb <*> arb <*> arb <*> arb <*> arb <*> arb <*> arb <*> arb <*> arb

arbMaybe :: Gen a -> Gen (Maybe a)
arbMaybe g = oneof [pure Nothing, Just <$> g]

arbMaybe' :: Int -> Int -> Gen a -> Gen (Maybe a)
arbMaybe' j n g = oneof $ replicate j (Just <$> g) <> replicate n (pure Nothing)

someOf :: Int -> Int -> Gen a -> Gen (V.Vector a)
someOf n m g = (`V.replicateM` g) =<< elements [n..m]

arbMV :: Arbitrary a => Gen (Maybe (V.Vector a))
arbMV = arbMaybe $ (`V.replicateM` arb) =<< elements [1,2,2,2,2]

----------------------------------------------------------------------
-- arbitrary readable text

-- | source: lipsum.com
loremIpsum :: [ST]
loremIpsum = ST.unlines <$>
  [ -- The standard Lorem Ipsum passage, used since the 1500s
   ["Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do"
   ,"eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad"
   ,"minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip"
   ,"ex ea commodo consequat. Duis aute irure dolor in reprehenderit in"
   ,"voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur"
   ,"sint occaecat cupidatat non proident, sunt in culpa qui officia"
   ,"deserunt mollit anim id est laborum."
   ]

  , -- Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC
   ["Sed ut perspiciatis unde omnis iste natus error sit voluptatem"
   ,"accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae"
   ,"ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt"
   ,"explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut"
   ,"odit aut fugit, sed quia consequuntur magni dolores eos qui ratione"
   ,"voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum"
   ,"quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam"
   ,"eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat"
   ,"voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam"
   ,"corporis suscipit laboriosam, nisi ut aliquid ex ea commodi"
   ,"consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate"
   ,"velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum"
   ,"fugiat quo voluptas nulla pariatur?"
   ]

  , -- 1914 translation by H. Rackham
   ["But I must explain to you how all this mistaken idea of denouncing"
   ,"pleasure and praising pain was born and I will give you a complete"
   ,"account of the system, and expound the actual teachings of the great"
   ,"explorer of the truth, the master-builder of human happiness. No one"
   ,"rejects, dislikes, or avoids pleasure itself, because it is pleasure,"
   ,"but because those who do not know how to pursue pleasure rationally"
   ,"encounter consequences that are extremely painful. Nor again is there"
   ,"anyone who loves or pursues or desires to obtain pain of itself,"
   ,"because it is pain, but because occasionally circumstances occur in"
   ,"which toil and pain can procure him some great pleasure. To take a"
   ,"trivial example, which of us ever undertakes laborious physical"
   ,"exercise, except to obtain some advantage from it? But who has any"
   ,"right to find fault with a man who chooses to enjoy a pleasure that has"
   ,"no annoying consequences, or one who avoids a pain that produces no"
   ,"resultant pleasure?"
   ]

  , -- Section 1.10.33 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC
   ["At vero eos et accusamus et iusto odio dignissimos ducimus qui"
   ,"blanditiis praesentium voluptatum deleniti atque corrupti quos dolores"
   ,"et quas molestias excepturi sint occaecati cupiditate non provident,"
   ,"similique sunt in culpa qui officia deserunt mollitia animi, id est"
   ,"laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita"
   ,"distinctio. Nam libero tempore, cum soluta nobis est eligendi optio"
   ,"cumque nihil impedit quo minus id quod maxime placeat facere possimus,"
   ,"omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem"
   ,"quibusdam et aut officiis debitis aut rerum necessitatibus saepe"
   ,"eveniet ut et voluptates repudiandae sint et molestiae non recusandae."
   ,"Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis"
   ,"voluptatibus maiores alias consequatur aut perferendis doloribus"
   ,"asperiores repellat."
   ]

  , -- 1914 translation by H. Rackham
   ["On the other hand, we denounce with righteous indignation and dislike"
   ,"men who are so beguiled and demoralized by the charms of pleasure of"
   ,"the moment, so blinded by desire, that they cannot foresee the pain and"
   ,"trouble that are bound to ensue; and equal blame belongs to those who"
   ,"fail in their duty through weakness of will, which is the same as"
   ,"saying through shrinking from toil and pain. These cases are perfectly"
   ,"simple and easy to distinguish. In a free hour, when our power of"
   ,"choice is untrammelled and when nothing prevents our being able to do"
   ,"what we like best, every pleasure is to be welcomed and every pain"
   ,"avoided. But in certain circumstances and owing to the claims of duty"
   ,"or the obligations of business it will frequently occur that pleasures"
   ,"have to be repudiated and annoyances accepted. The wise man therefore"
   ,"always holds in these matters to this principle of selection: he"
   ,"rejects pleasures to secure other greater pleasures, or else he endures"
   ,"pains to avoid worse pains."
   ]

  , -- 25 most common adjectives according to the Oxford English Dictionary.
   ["good", "new", "first", "last", "long", "great", "little"
   ,"own", "other", "old", "right", "big", "high", "different"
   ,"small", "large", "next", "early", "young", "important"
   ,"few", "public", "bad", "same", "able"
   ]
  ]

loremIpsumDict :: [ST]
loremIpsumDict = nub . sort . mconcat $ ST.words <$> loremIpsum

arbHex :: Int -> Gen ST
arbHex len = cs <$> replicateM len (elements $ ['0'..'9'] <> ['A'..'F'])

arbWord :: Gen ST
arbWord = ST.filter isAlpha <$> elements loremIpsumDict

arbPhrase :: Gen NonBlank
arbPhrase = unsafeNonBlank "arbPhrase" <$> do
    n <- (+ 3) . (`mod` 5) <$> arbitrary
    ST.intercalate " " <$> replicateM n arbWord

arbParagraph :: Gen NonBlank
arbParagraph = mk . (+ 13) . abs =<< arb
  where
    mk :: Int -> Gen NonBlank
    mk n = unsafeNonBlank "arbParagraph" . terminate . ST.unwords <$> replicateM n (elements loremIpsumDict)

    terminate :: ST -> ST
    terminate xs = (if isAlpha $ ST.last xs then ST.init xs else xs) <> "."

instance Arbitrary PersonQuery where
  arbitrary = PersonQuery <$> pure nil {- FIXME still better than generating broken IDs -}
                          <*> arbMV <*> arbMV <*> arbMV

arbPerson :: Gen Person
arbPerson = do
  someGender <- arb
  let arbFirstname = elements (if someGender ^. _Gender . nonBlank == "H" then boys else girls)
  Person <$> pure someGender <*> arb <*> arbFirstname <*> arb <*> arb <*> arbMaybe' 1 9 arb

instance Arbitrary a => Arbitrary (Comparator a) where arbitrary = elements allComparators <*> arb
instance Arbitrary Admin where arbitrary = arb3 Admin
instance Arbitrary ViewAdmin where arbitrary = arb2 ViewAdmin
instance Arbitrary LoginData where arbitrary = arb2 LoginData
instance Arbitrary UpdateAdmin where arbitrary = arb3 UpdateAdmin

addDBArb :: (AddDB k, MonadPersist m, MonadIO m)
         => DBMap k -> Gen Username -> Gen (NewData k) -> m (StoreRecord k)
addDBArb fld arbAuthor arbData = do
  author <- liftIO (generate arbAuthor)
  addDBStoreRecord (AuthEnv author) fld =<< liftIO (generate arbData)

devRootUpdateAdmin :: UpdateAdmin
devRootUpdateAdmin = UpdateAdmin (Login "root") (EmailAddress "TODO") (Just $ Password "TODO")

populateDB :: (MonadIO m, MonadPersist m) => Int -> m ()
populateDB s = do
  let root = devRootUpdateAdmin
  void $ addDBStoreRecord (root ^. username . to AuthEnv) Admins root
  void $ replicateM (4 * s) $ addDBArb Admins (pure (root ^. username)) arb

instance Arbitrary NonBlank where
  arbitrary = (<>?) <$> elements heads <*> arb
    where
      heads = unsafeNonBlank "Arbitrary.NonBlank" <$> ["AA","BB"]

instance Arbitrary Lastname where
  arbitrary = elements lastnames

instance Arbitrary Gender where
  arbitrary = elements genders

instance Arbitrary Timestamp where
  arbitrary = arb1 Timestamp

instance Arbitrary EmailSubject where
  arbitrary = EmailSubject <$> arbPhrase

instance Arbitrary EmailBody where
  arbitrary = EmailBody <$> arbParagraph

instance Arbitrary EmailSignature where
  arbitrary = pure . EmailSignature . unsafeNonBlank "Arbitrary.EmailSignature" $ "-- Best regards"

instance Arbitrary HashedPassword where
  arbitrary = unsafePerformIO . hashPassword <$> arb -- Nah, that's ok

instance Arbitrary AuthToken where
  arbitrary = AuthToken <$> arbHex 32

instance Arbitrary Username where
  arbitrary = Username <$> arbWord

instance Arbitrary Login where
  arbitrary = Login <$> arbWord

instance Arbitrary EmailAddress where
  arbitrary = mkEmailAddress <$> arbWord <*> arbWord <*> elements someMailinatorDomains
    where
      someMailinatorDomains =
        ["spambooger.com"
        ,"suremail.info"
        ,"reconmail.com"
        ,"bobmail.info"
        ,"safetymail.info"
        ,"streetwisemail.com"
        ,"devnullmail.com"
        ,"veryrealemail.com"
        ]
      mkEmailAddress firstn lastn domain =
        EmailAddress . mconcat $ ["TODO.", firstn, ".", lastn, "@", domain]

instance Arbitrary Birthdate where
  arbitrary = mk <$> elements [1950..2010] <*> elements [1..12] <*> elements [1..29]
    where
      mk year month day = Birthdate $ fromGregorian year month day

instance Arbitrary Eventdate where
  arbitrary = mk <$> elements [1990..2016] <*> elements [1..12] <*> elements [1..29]
    where
      mk year month day = Eventdate $ fromGregorian year month day

instance Arbitrary Year where
  arbitrary = Year <$> elements [2000..2016]

instance Arbitrary Days where
  arbitrary = Days <$> elements [0..5]

instance Arbitrary Count where
  arbitrary = Count <$> elements [0..5]

instance Arbitrary Password where
  arbitrary = mk <$> arbWord <*> elements [0..100 :: Int]
    where
      mk x y = Password $ x <> cs (show y)

-- From https://fr.wikipedia.org/wiki/Liste_des_pr%C3%A9noms_les_plus_donn%C3%A9s_en_France
firstnames, boys, girls :: [Firstname]
boys = Firstname . unsafeNonBlank "boys" <$>
  ["Jean","Philippe","Michel","Alain","Patrick","Nicolas","Christophe","Pierre","Christian"
  ,"Éric","Frédéric","Laurent","Stéphane","David","Pascal","Daniel","Sébastien","Julien"
  ,"Thierry","Olivier","Bernard","Thomas","Alexandre","Gérard","Didier","Dominique","Vincent"
  ,"François","Bruno","Guillaume","Jérôme","Jacques","Marc","Maxime","Romain","Claude","Antoine"
  ,"Franck","Jean-Pierre","Anthony","Kévin","Gilles","Cédric","Serge","André","Mathieu","Benjamin"
  ,"Patrice","Fabrice","Joël","Jérémy","Clément","Arnaud","Denis","Paul","Lucas","Hervé"
  ,"Jean-Claude","Sylvain","Yves","Ludovic","Guy","Florian","Damien","Alexis","Mickaël","Quentin"
  ,"Emmanuel","Louis","Benoît","Jean-Luc","Fabien","Francis","Hugo","Jonathan","Loïc","Xavier"
  ,"Théo","Adrien","Raphaël","Jean-Francois","Grégory","Robert","Michaël","Valentin","Cyril"
  ,"Jean-Marc","René","Lionel","Yannick","Enzo","Yannis","Jean-Michel","Baptiste","Matthieu"
  ,"Rémi","Georges","Aurélien","Nathan","Jean-Paul"]
girls = Firstname . unsafeNonBlank "girls" <$>
  ["Marie","Nathalie","Isabelle","Sylvie","Catherine","Laurie","Christine","Françoise","Valerie"
  ,"Sandrine","Stephanie","Veronique","Sophie","Celine","Chantal","Patricia","Anne","Brigitte"
  ,"Julie","Monique","Aurelie","Nicole","Laurence","Annie","Émilie","Dominique","Virginie","Corinne"
  ,"Elodie","Christelle","Camille","Caroline","Lea","Sarah","Florence","Laetitia","Audrey","Helene"
  ,"Laura","Manon","Michele","Cecile","Christiane","Beatrice","Claire","Nadine","Delphine","Pauline"
  ,"Karine","Melanie","Marion","Chloe","Jacqueline","Elisabeth","Evelyne","Marine","Claudine","Anais"
  ,"Lucie","Danielle","Carole","Fabienne","Mathilde","Sandra","Pascale","Annick","Charlotte","Emma"
  ,"Severine","Sabrina","Amandine","Myriam","Jocelyne","Alexandra","Angelique","Josiane","Joelle"
  ,"Agnes","Mireille","Vanessa","Justine","Sonia","Bernadette","Emmanuelle","Oceane","Amelie"
  ,"Clara","Maryse","Anne-marie","Fanny","Magali","Marie-christine","Morgane","Ines","Nadia"
  ,"Muriel","Jessica","Laure","Genevieve","Estelle"]
firstnames = boys <> girls

-- From https://fr.wikipedia.org/wiki/Liste_des_noms_de_famille_les_plus_courants_en_France
lastnames :: [Lastname]
lastnames = Lastname . unsafeNonBlank "lastnames" <$>
  ["Martin","Bernard","Thomas","Petit","Robert","Richard","Durand","Dubois"
  ,"Moreau","Laurent","Simon","Michel","Lefebvre","Leroy","Roux","David"
  ,"Bertrand","Morel","Fournier","Girard","Bonnet","Dupont","Lambert","Fontaine"
  ,"Rousseau","Vincent","Muller","Lefevre","Faure","Andre","Mercier","Blanc"
  ,"Guerin","Boyer","Garnier","Chevalier","Francois","Legrand","Gauthier","Garcia"
  ,"Perrin","Robin","Clement","Morin","Nicolas","Henry","Roussel","Mathieu"
  ,"Gautier","Masson","Marchand","Duval","Denis","Dumont","Marie","Lemaire","Noel"
  ,"Meyer","Dufour","Meunier","Brun","Blanchard","Giraud","Joly","Riviere","Lucas"
  ,"Brunet","Gaillard","Barbier","Arnaud","Martinez","Gerard","Roche","Renard"
  ,"Schmitt","Roy","Leroux","Colin","Vidal","Caron","Picard","Roger","Fabre"
  ,"Aubert","Lemoine","Renaud","Dumas","Lacroix","Olivier","Philippe","Bourgeois"
  ,"Pierre","Benoit","Rey","Leclerc","Payet","Rolland","Leclercq","Guillaume"
  ,"Lecomte"]

genders :: [Gender]
genders = Gender . unsafeNonBlank "genders" <$> ["H", "F"]
