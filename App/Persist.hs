{- Copyright (C) Nicolas Pouillard - All Rights Reserved
   Refer to the LICENSE file.
-}
{-# LANGUAGE ConstraintKinds   #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE GADTs             #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE Rank2Types        #-}
{-# LANGUAGE TemplateHaskell   #-}
module App.Persist where

import App.Core
import App.Enum (Enum, Option)
import Control.Monad (when, void)
import Control.Monad.Except (MonadError, throwError)
import Control.Monad.Reader (MonadReader)
import Control.Monad.State (MonadState, get)
import Control.Monad.Writer (MonadWriter, tell)
import Prelude hiding (Enum)
import Servant (ToHttpApiData, ServantErr(errBody), err403, err404, err409, err500)

newtype AuthEnv = AuthEnv
  { _currentUsername :: Username
  }

makeLenses ''AuthEnv

newtype PersistEnv = PersistEnv
  { _currentTimestamp :: {-!-}Timestamp
--  , _currentConfig    :: !Config
  }

makeLenses ''PersistEnv

class MonadUpdate m where
  getDB :: m DB
  updateDB :: Update -> m ()

  default getDB :: MonadState DB m => m DB
  getDB = get

  default updateDB :: (MonadState DB m, MonadWriter [Update] m) => Update -> m ()
  updateDB up = runUpdate up >> tell [up]

type MonadPersist m = (MonadReader PersistEnv m,
                       MonadUpdate m,
                       MonadError ServantErr m)

useDB :: MonadPersist m => Getting a DB a -> m a
useDB l = view l <$> getDB

findFirstDB :: MonadPersist m => Fold DB a -> m a
findFirstDB l = may404 =<< tryFindFirstDB l

tryFindFirstDB :: MonadPersist m => Fold DB a -> m (Maybe a)
tryFindFirstDB l = preview l <$> getDB

may404 :: MonadError ServantErr m => Maybe a -> m a
may404 Nothing  = throwError $ err404 { errBody = "missing record" }
may404 (Just x) = pure x

findAllDB :: Fold DB a -> MonadPersist m => m [a]
findAllDB l = toListOf l <$> getDB

cleanDB :: DB -> DB
cleanDB = admins .~ nil

dumpDB :: MonadPersist m => m DB
dumpDB = cleanDB <$> getDB

countDB :: Fold DB a -> MonadPersist m => m Count
countDB l = Count . lengthOf l <$> getDB

averageDB :: Num a => Fold DB a -> MonadPersist m => m (Average a)
averageDB l = averageOf l <$> getDB

{-
fractionalAverageDB :: Fractional a => Fold DB a -> MonadPersist m => m a
fractionalAverageDB l = fractionalAverageOf l <$> getDB

integralAverageDB :: Integral a => Fold DB a -> MonadPersist m => m a
integralAverageDB l = integralAverageOf l <$> getDB

-- FIXME: we should get an optimized version of that in lens
fractionalAverageOf :: Fractional a => Getting (Sum a, Sum a) s a -> s -> a
fractionalAverageOf l = getAverage . foldMapOf l ((,) (Sum 1) . Sum)
  where
    getAverage (Sum d, Sum n) = n / d

integralAverageOf :: Integral a => Getting (Sum a, Sum a) s a -> s -> a
integralAverageOf l = getAverage . foldMapOf l ((,) (Sum 1) . Sum)
  where
    getAverage (Sum d, Sum n) = n `div` d
-}

findAdminByLogin :: MonadPersist m => Login -> m (StoreRecord Admin)
findAdminByLogin l = findFirstDB $ admins . each . filtered ((== l) . view login)

invalidAuth :: MonadError ServantErr m => m a
invalidAuth = throwError $ err403 { errBody = "Invalid or missing authentication credentials." }

checkAuthToken :: (DBOf k ~ DB, IDOf k ~ ID k, HasSingleIDMap k,
                   HasUsername (StoreRecord k), HasAuthToken k (StoreRecord k), MonadPersist m)
               => AuthSeed -> Maybe (ID k) -> Maybe AuthToken -> m (AuthEnv, StoreRecord k)
checkAuthToken _    Nothing        _      = invalidAuth
checkAuthToken _    _        Nothing      = invalidAuth
checkAuthToken seed (Just i) (Just token) = do
  mr <- tryFindFirstDB $ singleIdMap (with i) . at i . _Just
  case mr of
    Just r | authToken seed r == token -> pure (r ^. username . to AuthEnv, r)
    _                                  -> invalidAuth

type ModifyRecordDB k = (DBOf k ~ DB, HasChanges k (StoreMeta k), ToHttpApiData (IDOf k),
                         ToJSON (StoreMeta k), ToJSON (StoreData k))

class Functor f => ToMaybe f where
  toMaybe :: f a -> Maybe a

instance ToMaybe Maybe where
  toMaybe = id

instance ToMaybe Identity where
  toMaybe = Just . runIdentity

modifyRecordDB'' :: (ModifyRecordDB k, MonadPersist m, ToMaybe f)
                 => AuthEnv -> DBMap k -> IDOf k -> ChangeType -> (DBOf k -> StoreRecord k -> f (StoreRecord k))
                 -> m (f (StoreRecord k))
modifyRecordDB'' (AuthEnv who) fld i ct up = do
  db <- getDB
  r <- may404 $ db ^. dbFieldLens fld . at i
  now <- view currentTimestamp
  let r' = up db r <&> _meta . changes Proxy %~ (Change who now ct :)
  updateDB . UpdateMap fld i $ toMaybe r'
  pure r'

modifyRecordDB' :: (ModifyRecordDB k, MonadPersist m)
                => AuthEnv -> DBMap k -> IDOf k -> ChangeType -> (DBOf k -> Endom (StoreRecord k)) -> m (StoreRecord k)
modifyRecordDB' a fld i ct up = fmap runIdentity $ modifyRecordDB'' a fld i ct (\db -> Identity . up db)

proxyDBMap :: DBMap k -> Proxy k
proxyDBMap _ = Proxy

modifyRecordDB :: (HasView k, HasUpdate k, ModifyRecordDB k, MonadPersist m)
               => AuthEnv -> DBMap k -> IDOf k -> UpdateData k -> m (ViewRecord k)
modifyRecordDB a fld i up =
  viewRecord500 proxy =<< modifyRecordDB' a fld i Update (updateRecord proxy up)
  where proxy = proxyDBMap fld

viewRecord500 :: (DBOf k ~ DB, HasView k, MonadPersist m) => proxy k -> StoreRecord k -> m (ViewRecord k)
viewRecord500 proxy r = do
  db <- getDB
  may500 $ db ^? viewRecord proxy r
  where
    -- FIXME: If this happens, this is likely to happen all the time after
    -- that. Hence we rollback the change we just did, which is what
    -- happens when an exception is thrown.
    may500 Nothing  = throwError $ err500 { errBody = "could not fetch the updated meta data" }
    may500 (Just x) = pure x

type ValidateDB k = (HasView k, ModifyRecordDB k)

validateRecordDB :: (ValidateDB k, MonadPersist m)
                 => AuthEnv -> DBMap k -> IDOf k -> m (ViewRecord k)
validateRecordDB who fld i =
  modifyRecordDB' who fld i Validation (const id) >>= viewRecord500 (proxyDBMap fld)

type DeleteDB k = (ModifyRecordDB k, HasDelete k)

deleteRecordDB :: (DeleteDB k, MonadPersist m) => AuthEnv -> DBMap k -> IDOf k -> m ()
deleteRecordDB a fld i = void $ modifyRecordDB'' a fld i Delete (deleteRecord (proxyDBMap fld))

restoreRecordDB :: (ModifyRecordDB k, MonadPersist m) => AuthEnv -> DBMap k -> IDOf k -> m ()
restoreRecordDB a fld i = void $ modifyRecordDB' a fld i Restore (const id)

notifyRecordDB :: (ModifyRecordDB k, MonadPersist m)
               => ID () -> AuthEnv -> DBMap k -> IDOf k -> m (StoreRecord k)
notifyRecordDB notifierId who fld i =
  modifyRecordDB' who fld i (Notification notifierId) (const id)

class HasFreshID k where
  freshID :: MonadPersist m => DBMap k -> NewData k -> m (IDOf k)

  default freshID :: (MonadPersist m, IDOf k ~ ID k) => DBMap k -> NewData k -> m (IDOf k)
  freshID fld _ = do
    i <- useDB nextID
    updateDB IncrNextID
    pure $ mkID (proxyDBMap fld) i

anyFreshID :: (MonadPersist m, HasID k (NewData k), ToHttpApiData (IDOf k)) => DBMap k -> NewData k -> m (IDOf k)
anyFreshID fld nd = do
  let i = nd ^. _ID
  m <- useDB (dbFieldLens fld . at i)
  when (m & has _Just) . throwError $ err409 { errBody = "Identifier already used" }
  pure i

instance HasFreshID Admin
instance HasFreshID Person
instance HasFreshID Enum where freshID = anyFreshID
instance HasFreshID Option where freshID = anyFreshID

type AddDBWithID k = (DBOf k ~ DB, HasNew k, HasView k, ToJSON (StoreMeta k), ToJSON (StoreData k), ToHttpApiData (IDOf k))
type AddDB k = (AddDBWithID k, HasFreshID k)

-- FIXME how/when to enforce uniqueness constraints?
-- Add a list of constraints: [UniqueConstraint] all constraints are then
-- matched with the new record against all the past records. This can be quite
-- slow but since we don't have any secondary indices this does not make any
-- difference. Actually adding such secondary indices would be an attractive
-- place to enforce the corresponding uniqueness constraint. While this would
-- be much faster with would cost more memory.
--
-- data UniqueConstraint k where
--   UniqueConstraint :: Eq a => Getter (StoreRecord k) (Maybe a) -> UniqueConstraint k
--
-- data SecondaryIndex k where
--   SecondaryIndex :: Hashable a => Getter (StoreRecord k) (Maybe a) -> SecondaryIndex k
addDBWithID :: (AddDBWithID k, MonadPersist m) => AuthEnv -> DBMap k -> NewData k -> IDOf k -> m (StoreRecord k)
addDBWithID (AuthEnv who) fld nd i = do
  now <- view currentTimestamp
  db <- getDB
  let m = Meta
            { _metaID        = i
            , _metaCreatedBy = who
            , _metaCreatedAt = now
            , _metaChanges   = []
            }
      r = newRecord (proxyDBMap fld) db $ Record m nd
  updateDB $ UpdateMap fld i (Just r)
  pure r

addDBStoreRecord :: (AddDB k, MonadPersist m) => AuthEnv -> DBMap k -> NewData k -> m (StoreRecord k)
addDBStoreRecord who fld nd = addDBWithID who fld nd =<< freshID fld nd

addDB :: (AddDB k, MonadPersist m) => AuthEnv -> DBMap k -> NewData k -> m (ViewRecord k)
addDB who fld nd = viewRecord500 (proxyDBMap fld) =<< addDBStoreRecord who fld nd

importDB :: (AddDB k, MonadPersist m) => AuthEnv -> DBMap k -> [NewData k] -> m [ViewRecord k]
importDB who = mapM . addDB who

assertEqual :: (MonadError ServantErr m, ToJSON a, Eq a) => LBS -> a -> a -> m ()
assertEqual msg x y
  | x == y    = return ()
  | otherwise = throwError err500 { errBody = err }

  where
    err = "assertion failed: " <> msg <> " (" <> encode x <> " vs " <> encode y <> ")"
