#!/bin/sh
#export NIX_PATH=nixpkgs=$HOME/hub/NixOS/nixpkgs-stack
export TMPDIR=$PWD/.stack-work/tmp
STACK_OPTS=(--docker)
#STACK_BUILD=(--file-watch)
STACK_BUILD=()

mkdir -p $TMPDIR

build(){
  stack "${STACK_OPTS[@]}" build --fast "${STACK_BUILD[@]}" --pedantic
}

backend(){
  stack "${STACK_OPTS[@]}" exec app-backend -- "$@"
}

dev(){
  sudo docker --version >/dev/null
  {
    sleep 5
    importlists
  } &
  backend dev
}

serve(){
  backend serve config.json
}

check(){
  backend . "$@"
}

repl(){
  stack "${STACK_OPTS[@]}" repl app-backend "$@"
}

diffapi(){
  rm -f app.swagger.api.yaml
  backend swagger "$@" | jq -S . | json2yaml > app.swagger.api.yaml
  nvim -d \
    <(jq -S . < app.swagger.api.json  | json2yaml) \
    app.swagger.api.yaml
}

BACKEND_URL="http://0.0.0.0:3080"
AUTH_ID=1
AUTH_TOKEN=TODO

appcurl(){
  local umeth="$1"
  local upath="$2"
  shift 2
  # Use
  #   -w '\ncode: %{http_code}'
  # to show the status code
  curl -H'Content-Type: application/json' \
       -H"X-Auth-Token: $AUTH_TOKEN" \
       -H"X-Auth-Id: $AUTH_ID" -s -X"$umeth" "$@" \
       "$BACKEND_URL/api/$upath"
}

appet(){
  appcurl GET "$@"
}

appost(){
  local upath="$1"
  shift
  appcurl POST "$upath" -d@- "$@"
}

importlist(){
  echo "Importing $1..."
  echo '{"id": "'"$1"'", "name": "'"$2"'"}' | apppost enums -v
  2>>importlist.log >>importlist.log
  local input="$(echo static_"$1".?son)"
  {
    case "$input" in
    (*.cson) cson2json;;
    (*.json) cat;;
    (*) echo "Error $input" >>/dev/stderr; return 1;;
    esac
  } < "$input" |
  jq 'map({id,name: .text})' |
  apppost enum/"$1"/options/import -v 2>>importlist.log >>importlist.log
}

importlists(){
  importlist genders 'Civilités'
}
