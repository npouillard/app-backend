FROM        debian:jessie
RUN         apt-get update -y && apt-get install -y libgmp10 ca-certificates
COPY        bin/app-backend /usr/local/bin/
VOLUME      /data
ENTRYPOINT  ["/usr/local/bin/app-backend"]
WORKDIR     /data
