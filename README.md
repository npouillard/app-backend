Requirements
============

* stack: http://haskellstack.org

Build
=====

    $ stack setup
    $ stack build

While you wait you can setup a configuration file, you can start
configuring...

Configuring
===========

    $ cp sample.config.json config.json

And then edit config.json to add your mailgun API key.

Now you need a database. You can get an empty database or one filled with
arbitrary data.

    # empty database
    $ stack exec app-backend -- empty > db-dump.json
    # random database
    $ stack exec app-backend -- arb 1 > db-dump.json

Running
=======

Then you can start the server with:

    $ stack exec app-backend -- serve config.json

The server now serves the REST API.
