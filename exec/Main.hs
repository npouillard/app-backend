{- Copyright (C) Nicolas Pouillard - All Rights Reserved
   Refer to the LICENSE file.
-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies     #-}
module Main where

import App.Api
import App.Core
import App.Arbitrary
import App.Server.Stack
import Control.Monad
import Data.Aeson (eitherDecode)
import Data.Default.Class
import Data.Swagger hiding (url, version)
import Data.Time.Calendar (Day)
import Data.Time.Clock (getCurrentTime, utctDay)
import Network.Wai (Middleware)
import Network.Wai.Handler.Warp
-- import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Network.Wai.Middleware.RequestLogger
import Network.Wai.Middleware.RequestLogger.JSON
import System.Log.FastLogger (fromLogStr)
import System.Environment
import System.Exit
import System.IO

import qualified Data.ByteString as SBS
import qualified Data.ByteString.Lazy.Char8 as LBS

showDB :: Either String DB -> String
showDB = show

mkLogging :: IO Middleware
mkLogging = mkRequestLogger (def { destination = Callback cb }) { outputFormat = CustomOutputFormatWithDetails formatAsJSON }
  where
    cb s = SBS.hPutStr stdout (fromLogStr s) >> hFlush stdout

putJSON :: ToJSON a => a -> IO ()
putJSON = LBS.putStrLn . encode

-- Does nothing for now...
checkDB :: DB -> DB
checkDB = id

readConfig :: FilePath -> IO Config
readConfig c = either (\m -> error $ "Cannot decode the configuration file `" <> c <> "`: " <> m) id
             . eitherDecode <$> LBS.readFile c

readConfigAndDb :: FilePath -> IO (Config, DB)
readConfigAndDb c = do
  cfg <- readConfig c
  -- FIXME this is not reading the log!
  db <- either error checkDB . eitherDecode <$> LBS.readFile (cfg ^. confDbDumpJson)
  pure (cfg, db)

-- FIXME this is not reading the log!
readDb :: Config -> IO DB
readDb cfg = either error checkDB . eitherDecode <$> LBS.readFile (cfg ^. confDbDumpJson)

readAuthToken :: (DBOf k ~ DB, HasSingleIDMap k, HasAuthToken k (StoreRecord k), IDOf k ~ ID k) => ID k -> Config -> IO AuthToken
readAuthToken i cfg = do
  db <- readDb cfg
  pure $ authToken (cfg ^. confAuthSeed) (db ^?! singleIdMap (with i) . at i . _Just)

mkPasswd :: IO HashedPassword
mkPasswd = do
  hSetBuffering stdin  NoBuffering
  hSetBuffering stderr NoBuffering
  hPutStrLn stderr "New password:"
  p0 <- starecho
  hPutStrLn stderr "Again:"
  p1 <- starecho
  when (p0 /= p1) $ do
    hPutStrLn stderr "Passwords do not match"
    exitFailure
  hashPassword . Password $ cs p0

-- From nptools/starecho.hs
starecho :: IO String
starecho = do
  hSetEcho stdin False
  p <- loop ""
  hSetEcho stdin True
  pure p

  where
    loop s = do c <- getChar
                proc c s

    -- tells what to do with the current char
    proc '\n'   = finish
    proc '\DEL' = del
    proc '\b'   = del
    proc c      = lineAndLoop c

    -- show a line and continue reading
    lineAndLoop c s = puts (showLine c (length s)) >> loop (c : s)

    showLine c n = '\r' : replicate n '*' ++ [c]

    -- Erase a char, visually and internally.
    del (_ : c : s) = erase >> proc c s
    del [_]         = erase >> loop ""
    del []          = loop ""

    -- Clear the line and return the string
    finish s = do puts $ '\r' : replicate (length s) ' ' ++ ['\r']
                  return $ reverse s

    -- To erase the previous char, one first go back, draw a space,
    -- and go back again.
    erase = puts "\b \b"

    puts = hPutStr stderr

url :: Config -> String
url cfg = "http://" <> cs (cfg ^. confListenHost) <> ":" <> show (cfg ^. confListenPort)

serve :: Config -> IO ()
serve cfg = do
  let settings = defaultSettings
                    & setHost (fromString . cs $ cfg ^. confListenHost)
                    & setPort (cfg ^. confListenPort)
  hPutStrLn stderr $ "The API url is: " <> url cfg
  -- hPutStrLn stderr $ "Browse the API at: " <> url cfg <> "/swagger-ui/index.html?url=" <> url cfg <> "/swagger.json#/default"
  logging <- mkLogging
  runSettings settings . logging =<< mkApp cfg

onDB :: Endom DB -> IO ()
onDB f = putJSON . either error f . eitherDecode =<< LBS.getContents

inlineSwagger :: Endom Swagger
inlineSwagger s = s & inlineNonRecursiveSchemas defs
                    & definitions .~ nil
  where defs = s ^. definitions

devServe :: IO ()
devServe = do
  let
    cfg = devConfig
    token = authToken (cfg ^. confAuthSeed) (AuthTokenData (ID 1 :: ID Admin) (devRootUpdateAdmin ^. emailAddress) (Nonce 0))
  hPutStrLn stderr "Config:"
  putJSON cfg
  hPutStrLn stderr $ "Token: " <> cs (fromAuthToken token)
  hPutStrLn stderr $ "Usage: " <> "curl -H'Content-Type: application/json"
                               <> "' -H'X-Auth-Token: " <> cs (fromAuthToken token)
                               <> "' -H'X-Auth-Id: 1' '" <> url cfg <> "/api/people'"
  serve cfg

main :: IO ()
main = do
  args <- getArgs
  case args of
    ["."]       -> onDB checkDB
--  ["api-js"]  -> putStrLn apiJS
    ["swagger"] -> putJSON swaggerApi
    ["swagger","--inline"] -> putJSON . inlineSwagger $ swaggerApi
    ["empty"]   -> putJSON emptyDB
    ["config"]  -> putJSON =<< generateConfig
    ["arb",s]   -> putJSON =<< generateRandomDB (read s)
    ["token",'@':i,c] -> putJSON =<< readAuthToken (ID (read i) :: ID Admin)  =<< readConfig c
    ["token",    i,c] -> putJSON =<< readAuthToken (ID (read i) :: ID Person) =<< readConfig c
    ["passwd"]  -> putJSON =<< mkPasswd
    ["serve",c] -> serve =<< readConfig c
    ["dev"]     -> devServe
    _           -> hPutStrLn stderr (unlines usage) >> exitFailure
  where
    usage =
      [ "Commands: app-backend [ serve | arb | empty | config | token | . | swagger [--inline] | dev ]"
      , ""
      , "  app-backend config > config.json      # Generates a configuration file with a random seed."
      , "  app-backend arb 1 > db.log            # Generates a random DB with a scaling factor of 1."
      , "  app-backend serve config.json         # Run the server using the given configuration file."
      , "  app-backend empty > emptydb.json      # Generates an empty DB."
      , "  app-backend . < in.json > out.json    # Read a DB on stdin and write it back on stdout."
      , "  app-backend token ID config.json      # Show the authentication token for the person with this ID."
      , "                                        # Use @ID for admins."
      , "  app-backend passwd                    # Hash your password."
      , "  app-backend swagger [--inline]        # Swagger specification of the API."
      , "  app-backend dev                       # Run the server with a random DB."
      ]
